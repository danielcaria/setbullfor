object frmTransportes: TfrmTransportes
  Left = 0
  Top = 0
  Caption = 'Adicionar Transporte'
  ClientHeight = 368
  ClientWidth = 728
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblPedido: TLabel
    Left = 402
    Top = 247
    Width = 94
    Height = 24
    Caption = 'lblPedido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16744448
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblMsg: TLabel
    Left = 8
    Top = 247
    Width = 388
    Height = 24
    Caption = 'Atribuir transportadora para o pedido:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 16744448
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dbgrdTransporte: TDBGrid
    Left = 8
    Top = 40
    Width = 712
    Height = 201
    DataSource = dsTransportadora
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'trans_codigo'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_nome'
        Title.Caption = 'Nome'
        Width = 300
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_telefone'
        Title.Caption = 'Telefone'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_cnpj'
        Title.Caption = 'CNPJ'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_endereco'
        Title.Caption = 'Endere'#231'o'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_municipio'
        Title.Caption = 'Cidade'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_cep'
        Title.Caption = 'CEP'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_uf'
        Title.Caption = 'UF'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_inscr_estadual'
        Title.Caption = 'Inscri'#231#227'o Estadual'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_inscr_municipal'
        Title.Caption = 'Inscri'#231#227'o Municipal'
        Visible = True
      end>
  end
  object edtIncrSearch: TEdit
    Left = 159
    Top = 8
    Width = 257
    Height = 21
    TabOrder = 1
  end
  object cbbFields: TComboBox
    Left = 8
    Top = 8
    Width = 145
    Height = 21
    ItemIndex = 0
    TabOrder = 0
    Text = 'Codigo'
    Items.Strings = (
      'Codigo'
      'Nome')
  end
  object btnLocalizar: TButton
    Left = 422
    Top = 8
    Width = 115
    Height = 25
    Caption = 'Localizar'
    TabOrder = 2
    OnClick = btnLocalizarClick
  end
  object btnAtualizar: TButton
    Left = 8
    Top = 286
    Width = 257
    Height = 74
    Caption = 'Atualizar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 12615680
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = btnAtualizarClick
  end
  object btnFechar: TButton
    Left = 463
    Top = 286
    Width = 257
    Height = 74
    Caption = 'Fechar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 12615680
    Font.Height = -27
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = btnFecharClick
  end
  object qryTransportadora: TUniQuery
    SQL.Strings = (
      'select * from tbtransportadora'
      'order by trans_nome')
    Left = 144
    Top = 264
  end
  object Transacao: TUniTransaction
    Left = 336
    Top = 120
  end
  object dsTransportadora: TUniDataSource
    DataSet = qryTransportadora
    Left = 488
    Top = 120
  end
  object sqlUpdate: TUniSQL
    Left = 416
    Top = 120
  end
end
