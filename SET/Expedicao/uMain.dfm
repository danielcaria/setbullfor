object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Expedi'#231#227'o de Produtos'
  ClientHeight = 521
  ClientWidth = 999
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 999
    Height = 73
    Align = alTop
    TabOrder = 0
    object lblCampo: TLabel
      Left = 16
      Top = 8
      Width = 96
      Height = 13
      Caption = 'Selecione um campo'
    end
    object lblOperador: TLabel
      Left = 151
      Top = 8
      Width = 46
      Height = 13
      Caption = 'Operador'
    end
    object lblValor: TLabel
      Left = 279
      Top = 8
      Width = 24
      Height = 13
      Caption = 'Valor'
    end
    object lblFilial: TLabel
      Left = 403
      Top = 8
      Width = 20
      Height = 13
      Caption = 'Filial'
    end
    object cbbCampo: TComboBox
      Left = 16
      Top = 27
      Width = 129
      Height = 22
      AutoComplete = False
      Style = csOwnerDrawFixed
      ItemIndex = 0
      TabOrder = 1
      Text = 'N'#250'mero do Pedido'
      Items.Strings = (
        'N'#250'mero do Pedido'
        'Cliente')
    end
    object edtCriterio: TEdit
      Left = 279
      Top = 27
      Width = 118
      Height = 21
      TabOrder = 3
    end
    object btnLocalizar: TButton
      Left = 554
      Top = 25
      Width = 75
      Height = 25
      Caption = 'Buscar'
      TabOrder = 0
      OnClick = btnLocalizarClick
    end
    object chkFiltroAtivado: TCheckBox
      Left = 19
      Top = 51
      Width = 97
      Height = 17
      Caption = 'Filtro Ativo?'
      TabOrder = 5
      OnClick = chkFiltroAtivadoClick
    end
    object cbbCondicao: TComboBox
      Left = 151
      Top = 27
      Width = 122
      Height = 22
      AutoComplete = False
      Style = csOwnerDrawFixed
      TabOrder = 2
      Items.Strings = (
        'Igual'
        'Diferente'
        'Maior'
        'Menor'
        'Maior ou igual'
        'Menor ou igual'
        'Cont'#233'm'
        'Inicia com'
        'Termina com'
        '')
    end
    object cbbFilial: TComboBox
      Left = 403
      Top = 27
      Width = 145
      Height = 21
      TabOrder = 4
      OnChange = cbbFilialChange
      Items.Strings = (
        'S'#227'o Paulo'
        'Curitiba')
    end
  end
  object pnlBotton: TPanel
    Left = 0
    Top = 477
    Width = 999
    Height = 44
    Align = alBottom
    TabOrder = 3
    object btnSalvar: TButton
      Left = 16
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Salvar'
      Enabled = False
      TabOrder = 0
      OnClick = btnSalvarClick
    end
    object btnEnviarEmail: TButton
      Left = 97
      Top = 6
      Width = 80
      Height = 25
      Caption = 'Enviar e-mail'
      TabOrder = 1
      OnClick = btnEnviarEmailClick
    end
    object btnAlterarTransp: TButton
      Left = 856
      Top = 6
      Width = 105
      Height = 25
      Caption = 'Alterar Transporte'
      TabOrder = 3
      OnClick = btnAlterarTranspClick
    end
    object btnUpdate: TButton
      Left = 745
      Top = 6
      Width = 105
      Height = 25
      Caption = 'Atualizar Dados'
      TabOrder = 2
      OnClick = btnUpdateClick
    end
    object pgBarStatus: TsProgressBar
      Left = 208
      Top = 9
      Width = 531
      Height = 24
      TabOrder = 4
    end
  end
  object pnlLeft: TPanel
    Left = 0
    Top = 73
    Width = 999
    Height = 263
    Align = alClient
    TabOrder = 1
    object DBGrid1: TDBGrid
      Left = 1
      Top = 142
      Width = 997
      Height = 120
      Align = alBottom
      DataSource = dsItens
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'epedi_cod_produto'
          Title.Caption = 'C'#243'd. Produto'
          Width = 142
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'prod_descricao'
          Title.Caption = 'Produto'
          Width = 264
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'epedi_qnt_solicitada'
          Title.Caption = 'Qnt. Solicitada'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'epedi_qnt_liberada'
          Title.Caption = 'Qnt. Liberada'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'epedi_qnt_faturada'
          Title.Caption = 'Qnt. Faturada'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'epedi_qnt_pendente'
          Title.Caption = 'Qnt. Pendente'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'epedi_valor_unit'
          Title.Caption = 'Valor Unit.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'epedi_valor_total'
          Title.Caption = 'Valor Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'epedi_qnt_volumes'
          Title.Caption = 'Qnt. Volumes'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'epedi_peso_bruto'
          Title.Caption = 'Peso Bruto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'epedi_peso_liquido'
          Title.Caption = 'Peso Liquido'
          Visible = True
        end>
    end
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 997
      Height = 141
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsEpedidos
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.CopyCaptionsToClipboard = False
        OptionsBehavior.CopyRecordsToClipboard = False
        OptionsBehavior.ImmediateEditor = False
        OptionsBehavior.IncSearch = True
        OptionsBehavior.CopyPreviewToClipboard = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsView.CellEndEllipsis = True
        OptionsView.GridLineColor = clGradientActiveCaption
        OptionsView.GroupByBox = False
        OptionsView.HeaderEndEllipsis = True
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 20
        OptionsView.RowSeparatorColor = clNavy
        object cxGrid1DBTableView1eped_codigo: TcxGridDBColumn
          Caption = 'C'#243'digo Pedido'
          DataBinding.FieldName = 'eped_codigo'
        end
        object cxGrid1DBTableView1nome_cliente: TcxGridDBColumn
          Caption = 'Cliente'
          DataBinding.FieldName = 'nome_cliente'
          Width = 272
        end
        object cxGrid1DBTableView1eped_contato_cliente: TcxGridDBColumn
          Caption = 'Contato'
          DataBinding.FieldName = 'eped_contato_cliente'
          Width = 120
        end
        object cxGrid1DBTableView1eped_emissao: TcxGridDBColumn
          Caption = 'Emiss'#227'o'
          DataBinding.FieldName = 'eped_emissao'
          Width = 69
        end
        object cxGrid1DBTableView1eped_entrega: TcxGridDBColumn
          Caption = 'Entrega'
          DataBinding.FieldName = 'eped_entrega'
          Width = 71
        end
        object cxGrid1DBTableView1eped_frete_resp: TcxGridDBColumn
          Caption = 'Respons'#225'vel Frete'
          DataBinding.FieldName = 'eped_frete_resp'
          Width = 107
        end
        object cxGrid1DBTableView1eped_dta_expedicao: TcxGridDBColumn
          Caption = 'Dta. Expedi'#231#227'o'
          DataBinding.FieldName = 'eped_dta_expedicao'
          Width = 98
        end
        object cxGrid1DBTableView1eped_observacoes: TcxGridDBColumn
          Caption = 'Observa'#231#245'es'
          DataBinding.FieldName = 'eped_observacoes'
        end
        object cxGrid1DBTableView1trans_nome: TcxGridDBColumn
          Caption = 'Transportadora'
          DataBinding.FieldName = 'trans_nome'
          Width = 200
        end
        object cxGrid1DBTableView1trans_telefone: TcxGridDBColumn
          Caption = 'Tel Transportadora'
          DataBinding.FieldName = 'trans_telefone'
        end
        object cxGrid1DBTableView1trans_email: TcxGridDBColumn
          Caption = 'E-mail Transportadora'
          DataBinding.FieldName = 'trans_email'
          Width = 120
        end
        object cxGrid1DBTableView1mun_municipio: TcxGridDBColumn
          Caption = 'Cidade'
          DataBinding.FieldName = 'mun_municipio'
          Width = 120
        end
        object cxGrid1DBTableView1est_uf: TcxGridDBColumn
          Caption = 'Estado'
          DataBinding.FieldName = 'est_uf'
        end
        object cxGrid1DBTableView1end_endereco: TcxGridDBColumn
          Caption = 'Endere'#231'o'
          DataBinding.FieldName = 'end_endereco'
          Width = 120
        end
        object cxGrid1DBTableView1end_bairro: TcxGridDBColumn
          Caption = 'Bairro'
          DataBinding.FieldName = 'end_bairro'
          Width = 120
        end
        object cxGrid1DBTableView1end_cep: TcxGridDBColumn
          Caption = 'CEP'
          DataBinding.FieldName = 'end_cep'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        Caption = 'Pedidos Aguardando envio'
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object pnlRigth: TPanel
    Left = 0
    Top = 336
    Width = 999
    Height = 141
    Align = alBottom
    TabOrder = 2
    object Label4: TLabel
      Left = 16
      Top = 20
      Width = 100
      Height = 13
      Caption = 'N'#250'mero CTe / Coleta'
    end
    object Label6: TLabel
      Left = 166
      Top = 16
      Width = 63
      Height = 13
      Caption = 'Observa'#231#245'es'
    end
    object sDBEdit1: TsDBEdit
      Left = 16
      Top = 39
      Width = 113
      Height = 21
      DataField = 'eped_cte_coleta'
      DataSource = dsEpedidos
      TabOrder = 1
      SkinData.SkinSection = 'EDIT'
    end
    object sdbmeped_observacoes: TsDBMemo
      Left = 166
      Top = 35
      Width = 795
      Height = 78
      DataField = 'eped_observacoes'
      DataSource = dsEpedidos
      TabOrder = 0
      SkinData.SkinSection = 'EDIT'
    end
  end
  object connData: TUniConnection
    ProviderName = 'PostgreSQL'
    Database = 'postgres'
    Username = 'postgres'
    Server = '192.168.0.2'
    LoginPrompt = False
    Left = 757
    Top = 360
    EncryptedPassword = 'CDFFCAFFCEFFCDFFCCFFCEFF'
  end
  object pstgrsqlnprvdr1: TPostgreSQLUniProvider
    Left = 595
    Top = 360
  end
  object qryEpedido: TUniQuery
    SQLInsert.Strings = (
      'INSERT INTO epedido'
      
        '  (eped_id, eped_codigo, eped_emissao, eped_entrega, eped_cod_cl' +
        'iente, eped_situacao, eped_transp, eped_frete_resp, eped_pagamen' +
        'to, eped_cancelado, eped_analise, eped_faturado, eped_parcial, e' +
        'ped_pendente, eped_vendedor, eped_contato_cliente, eped_email_co' +
        'ntato, eped_valor_total, eped_valor_st, eped_aliq_st, eped_valor' +
        '_icms, eped_aliq_icms, eped_valor_ipi, eped_aliq_ipi, eped_valor' +
        '_itens, eped_separador, eped_conferencia, eped_expedido, eped_dt' +
        'a_expedicao, eped_observacoes, eped_cancelado_send, eped_analise' +
        '_send, eped_faturado_send, eped_parcial_send, eped_pendente_send' +
        ', eped_trans_send, eped_cte_coleta)'
      'VALUES'
      
        '  (:eped_id, :eped_codigo, :eped_emissao, :eped_entrega, :eped_c' +
        'od_cliente, :eped_situacao, :eped_transp, :eped_frete_resp, :epe' +
        'd_pagamento, :eped_cancelado, :eped_analise, :eped_faturado, :ep' +
        'ed_parcial, :eped_pendente, :eped_vendedor, :eped_contato_client' +
        'e, :eped_email_contato, :eped_valor_total, :eped_valor_st, :eped' +
        '_aliq_st, :eped_valor_icms, :eped_aliq_icms, :eped_valor_ipi, :e' +
        'ped_aliq_ipi, :eped_valor_itens, :eped_separador, :eped_conferen' +
        'cia, :eped_expedido, :eped_dta_expedicao, :eped_observacoes, :ep' +
        'ed_cancelado_send, :eped_analise_send, :eped_faturado_send, :epe' +
        'd_parcial_send, :eped_pendente_send, :eped_trans_send, :eped_cte' +
        '_coleta)')
    SQLDelete.Strings = (
      'DELETE FROM epedido'
      'WHERE'
      '  eped_id = :Old_eped_id')
    SQLUpdate.Strings = (
      'UPDATE epedido'
      'SET'
      
        '  eped_id = :eped_id, eped_codigo = :eped_codigo, eped_emissao =' +
        ' :eped_emissao, eped_entrega = :eped_entrega, eped_cod_cliente =' +
        ' :eped_cod_cliente, eped_situacao = :eped_situacao, eped_transp ' +
        '= :eped_transp, eped_frete_resp = :eped_frete_resp, eped_pagamen' +
        'to = :eped_pagamento, eped_cancelado = :eped_cancelado, eped_ana' +
        'lise = :eped_analise, eped_faturado = :eped_faturado, eped_parci' +
        'al = :eped_parcial, eped_pendente = :eped_pendente, eped_vendedo' +
        'r = :eped_vendedor, eped_contato_cliente = :eped_contato_cliente' +
        ', eped_email_contato = :eped_email_contato, eped_valor_total = :' +
        'eped_valor_total, eped_valor_st = :eped_valor_st, eped_aliq_st =' +
        ' :eped_aliq_st, eped_valor_icms = :eped_valor_icms, eped_aliq_ic' +
        'ms = :eped_aliq_icms, eped_valor_ipi = :eped_valor_ipi, eped_ali' +
        'q_ipi = :eped_aliq_ipi, eped_valor_itens = :eped_valor_itens, ep' +
        'ed_separador = :eped_separador, eped_conferencia = :eped_confere' +
        'ncia, eped_expedido = :eped_expedido, eped_dta_expedicao = :eped' +
        '_dta_expedicao, eped_observacoes = :eped_observacoes, eped_cance' +
        'lado_send = :eped_cancelado_send, eped_analise_send = :eped_anal' +
        'ise_send, eped_faturado_send = :eped_faturado_send, eped_parcial' +
        '_send = :eped_parcial_send, eped_pendente_send = :eped_pendente_' +
        'send, eped_trans_send = :eped_trans_send, eped_cte_coleta = :epe' +
        'd_cte_coleta'
      'WHERE'
      '  eped_id = :Old_eped_id')
    SQLLock.Strings = (
      'SELECT * FROM epedido'
      'WHERE'
      '  eped_id = :Old_eped_id'
      'FOR UPDATE NOWAIT')
    SQLRefresh.Strings = (
      
        'SELECT eped_id, eped_codigo, eped_emissao, eped_entrega, eped_co' +
        'd_cliente, eped_situacao, eped_transp, eped_frete_resp, eped_pag' +
        'amento, eped_cancelado, eped_analise, eped_faturado, eped_parcia' +
        'l, eped_pendente, eped_vendedor, eped_contato_cliente, eped_emai' +
        'l_contato, eped_valor_total, eped_valor_st, eped_aliq_st, eped_v' +
        'alor_icms, eped_aliq_icms, eped_valor_ipi, eped_aliq_ipi, eped_v' +
        'alor_itens, eped_separador, eped_conferencia, eped_expedido, epe' +
        'd_dta_expedicao, eped_observacoes, eped_cancelado_send, eped_ana' +
        'lise_send, eped_faturado_send, eped_parcial_send, eped_pendente_' +
        'send, eped_trans_send, eped_cte_coleta FROM epedido'
      'WHERE'
      '  eped_id = :eped_id')
    SQLRecCount.Strings = (
      'SELECT count(*) FROM ('
      'SELECT * FROM epedido'
      ''
      ') t')
    Connection = connData
    SQL.Strings = (
      'select '
      'eped_id,'
      'eped_transp,'
      'eped_separador,'
      'eped_conferencia,'
      'eped_expedido,'
      'eped_situacao,'
      'eped_observacoes,'
      'eped_dta_expedicao,'
      'eped_cod_cliente,'
      'eped_cte_coleta,'
      'nome_cliente, '
      'eped_codigo, '
      'eped_emissao, '
      'eped_trans_send,'
      'eped_entrega, '
      'eped_frete_resp,'
      'eped_pagamento, '
      'tbtransportadora.trans_nome ,'
      'tbtransportadora.trans_telefone,'
      'tbtransportadora.trans_email, '
      'eped_vendedor, '
      'tbvendedor.email_vend,'
      'eped_contato_cliente, '
      'eped_email_contato,'
      'tbmunicipio.mun_municipio, '
      'tbestados.est_uf ,'
      'end_endereco,'
      'end_bairro,'
      'end_cep ,'
      'eped_cancelado,'
      'eped_analise,'
      'eped_faturado, '
      'eped_parcial ,'
      'eped_pendente,'
      'eped_cancelado_send,'
      'eped_analise_send,'
      'eped_faturado_send, '
      'eped_parcial_send,'
      'eped_pendente_send,'
      'eped_valor_total,'
      'eped_filial,'
      
        'eped_valor_st, eped_aliq_st, eped_valor_icms, eped_aliq_icms, ep' +
        'ed_valor_ipi, eped_aliq_ipi, eped_valor_itens,trans_codigo'
      'from epedido'
      ' left outer join tbcliente'
      ' on eped_cod_cliente = tbcliente.id_cliente'
      
        ' INNER join tbenderecos on tbenderecos.end_id = epedido.eped_end' +
        '_entrega'
      
        ' INNER join tbmunicipio on tbenderecos.end_municipio = tbmunicip' +
        'io.mun_id'
      
        ' left outer join tbtransportadora on epedido.eped_transp = tbtra' +
        'nsportadora.trans_codigo'
      
        ' inner join tbestados on tbmunicipio.mun_estado = tbestados.est_' +
        'id'
      
        ' left outer join tbvendedor on tbvendedor.codigo_vend = epedido.' +
        'eped_vendedor'
      'where eped_trans_send is null and eped_faturado_send = '#39'S'#39' '
      'and eped_emissao >= :emissao')
    Left = 838
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'emissao'
        Value = nil
      end>
    object qryEpedidoeped_id: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'eped_id'
    end
    object qryEpedidoeped_codigo: TStringField
      FieldName = 'eped_codigo'
      FixedChar = True
      Size = 12
    end
    object qryEpedidoeped_emissao: TDateField
      FieldName = 'eped_emissao'
    end
    object qryEpedidoeped_entrega: TDateField
      FieldName = 'eped_entrega'
    end
    object qryEpedidoeped_cod_cliente: TStringField
      FieldName = 'eped_cod_cliente'
      Size = 30
    end
    object qryEpedidoeped_situacao: TIntegerField
      FieldName = 'eped_situacao'
      Required = True
    end
    object qryEpedidoeped_transp: TStringField
      FieldName = 'eped_transp'
      FixedChar = True
      Size = 10
    end
    object qryEpedidoeped_frete_resp: TStringField
      FieldName = 'eped_frete_resp'
      FixedChar = True
      Size = 30
    end
    object qryEpedidoeped_pagamento: TStringField
      FieldName = 'eped_pagamento'
      Size = 100
    end
    object qryEpedidoeped_cancelado: TStringField
      FieldName = 'eped_cancelado'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_analise: TStringField
      FieldName = 'eped_analise'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_faturado: TStringField
      FieldName = 'eped_faturado'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_parcial: TStringField
      FieldName = 'eped_parcial'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_pendente: TStringField
      FieldName = 'eped_pendente'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_vendedor: TStringField
      FieldName = 'eped_vendedor'
      FixedChar = True
      Size = 10
    end
    object qryEpedidoeped_contato_cliente: TStringField
      FieldName = 'eped_contato_cliente'
      Size = 100
    end
    object qryEpedidoeped_email_contato: TStringField
      FieldName = 'eped_email_contato'
      Size = 255
    end
    object qryEpedidoeped_valor_total: TFloatField
      FieldName = 'eped_valor_total'
      DisplayFormat = '0.,0000#.,#### '
    end
    object qryEpedidoeped_valor_st: TFloatField
      FieldName = 'eped_valor_st'
    end
    object qryEpedidoeped_aliq_st: TFloatField
      FieldName = 'eped_aliq_st'
    end
    object qryEpedidoeped_valor_icms: TFloatField
      FieldName = 'eped_valor_icms'
    end
    object qryEpedidoeped_aliq_icms: TFloatField
      FieldName = 'eped_aliq_icms'
    end
    object qryEpedidoeped_valor_ipi: TFloatField
      FieldName = 'eped_valor_ipi'
    end
    object qryEpedidoeped_aliq_ipi: TFloatField
      FieldName = 'eped_aliq_ipi'
    end
    object qryEpedidoeped_valor_itens: TFloatField
      FieldName = 'eped_valor_itens'
    end
    object qryEpedidoeped_separador: TFloatField
      FieldName = 'eped_separador'
    end
    object qryEpedidoeped_conferencia: TFloatField
      FieldName = 'eped_conferencia'
    end
    object qryEpedidoeped_expedido: TFloatField
      FieldName = 'eped_expedido'
    end
    object qryEpedidoeped_dta_expedicao: TDateField
      FieldName = 'eped_dta_expedicao'
    end
    object qryEpedidoeped_observacoes: TMemoField
      FieldName = 'eped_observacoes'
      BlobType = ftMemo
    end
    object qryEpedidoeped_cancelado_send: TStringField
      FieldName = 'eped_cancelado_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_analise_send: TStringField
      FieldName = 'eped_analise_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_faturado_send: TStringField
      FieldName = 'eped_faturado_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_parcial_send: TStringField
      FieldName = 'eped_parcial_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_pendente_send: TStringField
      FieldName = 'eped_pendente_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidonome_cliente: TStringField
      FieldName = 'nome_cliente'
      ReadOnly = True
      Size = 255
    end
    object qryEpedidotrans_nome: TStringField
      FieldName = 'trans_nome'
      ReadOnly = True
      Size = 255
    end
    object qryEpedidoeped_trans_send: TStringField
      FieldName = 'eped_trans_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_cte_coleta: TStringField
      FieldName = 'eped_cte_coleta'
      Size = 30
    end
    object qryEpedidotrans_telefone: TStringField
      FieldName = 'trans_telefone'
      ReadOnly = True
      FixedChar = True
    end
    object qryEpedidotrans_email: TStringField
      FieldName = 'trans_email'
      ReadOnly = True
      Size = 255
    end
    object qryEpedidoemail_vend: TStringField
      FieldName = 'email_vend'
      ReadOnly = True
      Required = True
      Size = 200
    end
    object qryEpedidomun_municipio: TStringField
      FieldName = 'mun_municipio'
      ReadOnly = True
      Required = True
      Size = 120
    end
    object qryEpedidoest_uf: TStringField
      FieldName = 'est_uf'
      ReadOnly = True
      Required = True
      FixedChar = True
      Size = 2
    end
    object qryEpedidoend_endereco: TStringField
      FieldName = 'end_endereco'
      ReadOnly = True
      Required = True
      Size = 255
    end
    object qryEpedidoend_bairro: TStringField
      FieldName = 'end_bairro'
      ReadOnly = True
      Required = True
      Size = 255
    end
    object qryEpedidoend_cep: TStringField
      FieldName = 'end_cep'
      ReadOnly = True
      Required = True
      Size = 10
    end
    object qryEpedidoeped_filial: TStringField
      FieldName = 'eped_filial'
      FixedChar = True
      Size = 1
    end
    object strngfldEpedidotrans_codigo: TStringField
      FieldName = 'trans_codigo'
      ReadOnly = True
      FixedChar = True
      Size = 10
    end
  end
  object dsEpedidos: TDataSource
    DataSet = qryEpedido
    OnStateChange = dsEpedidosStateChange
    OnDataChange = dsEpedidosDataChange
    Left = 919
    Top = 279
  end
  object clMailMessage: TclMailMessage
    ToList = <>
    CCList = <>
    BCCList = <>
    Date = 42304.424277233800000000
    CharSet = 'iso-8859-1'
    ContentType = 'text/plain'
    Left = 676
    Top = 279
  end
  object clsmtp: TclSmtp
    OnProgress = clsmtpProgress
    MailAgent = 'Clever Internet Suite'
    Left = 757
    Top = 279
  end
  object qryItens: TUniQuery
    Connection = connData
    SQL.Strings = (
      
        'SELECT epedi_id, epedi_epedido, epedi_cod_produto, prod_descrica' +
        'o, epedi_qnt_solicitada, '
      
        '       epedi_qnt_liberada, epedi_qnt_faturada, epedi_qnt_pendent' +
        'e, epedi_valor_unit, '
      
        '       epedi_valor_total, epedi_valor_ttl_ipi, epedi_valor_liq_t' +
        'tl, '
      
        '       epedi_aliq_ipi, epedi_valor_ipi, epedi_aliq_icms, epedi_v' +
        'alor_icms, '
      
        '       epedi_aliq_icmsst, epedi_valor_icmsst, epedi_qnt_volumes,' +
        ' epedi_peso_bruto, '
      
        '       epedi_peso_liquido, epedi_qnt_ttl_volumes, epedi_peso_ttl' +
        'bruto, '
      '       epedi_peso_ttlliquido'
      '  FROM epeditem'
      
        'inner join tbproduto on epeditem.epedi_cod_produto = tbproduto.p' +
        'rod_codigo'
      'where epedi_qnt_solicitada > 0'
      'order by epedi_qnt_solicitada')
    MasterSource = dsEpedidos
    MasterFields = 'eped_codigo'
    DetailFields = 'epedi_epedido'
    Left = 919
    Top = 360
    ParamData = <
      item
        DataType = ftFixedChar
        Name = 'eped_codigo'
        ParamType = ptInput
        Value = '0616-000255'
      end>
  end
  object Timer1: TTimer
    Interval = 600000
    Left = 676
    Top = 360
  end
  object dsItens: TDataSource
    DataSet = qryItens
    Left = 838
    Top = 279
  end
  object ApplicationEvents1: TApplicationEvents
    OnException = ApplicationEvents1Exception
    Left = 595
    Top = 279
  end
end
