program Expedicao;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {frmMain},
  uSecurity in '..\uSecurity.pas',
  uTemplate in '..\uTemplate.pas',
  uAddTransporte in 'uAddTransporte.pas' {frmTransportes},
  uFuncoes in '..\uFuncoes.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
