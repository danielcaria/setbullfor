unit uSecurity;

interface

function Crypt(Opcao, Dados: String): String;


implementation

function Crypt(Opcao, Dados: String): String;
var
  I : Integer;
  Key : Word;
  Res : String;
const
  C1    = 335;
  C2    = 242;
  Chave = 368;
begin
  Key := Chave;
  for I := 1 to length(Dados) do
    begin
      Res := Res + Char(Byte(Dados[I]) xor (Key shr 8));
      if Opcao = 'CRYPT' then
        Key := (Byte(Res[I]) + Chave) * C1 + C2;
      if Opcao = 'DECRYPT' then
        Key := (Byte(Dados[I]) + Chave) * C1 + C2;
    end;
  Result := Res;
end;


end.
