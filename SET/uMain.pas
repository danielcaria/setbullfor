unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids,
  QImport3, QImport3ASCII, Vcl.ComCtrls, Data.DB, DBAccess, Uni, MemDS,
  UniProvider, acProgressBar, Vcl.DBGrids, acDBGrid, PostgreSQLUniProvider,
  DASQLMonitor, UniSQLMonitor, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxProgressBar, cxClasses,
  dxTaskbarProgress, shellApi, Vcl.Buttons, sBitBtn, clMailMessage, clTcpClient,
  clMC, clSmtp, Vcl.ExtCtrls, sSkinManager, iniFiles, Vcl.AppEvnts;

type
  TfrmMain = class(TForm)
    QImport3ASCII1: TQImport3ASCII;
    sProgressBar1: TsProgressBar;
    UniTransaction1: TUniTransaction;
    UniSQLMonitor1: TUniSQLMonitor;
    Updates_Ajustes: TUniSQL;
    PostgreSQLUniProvider1: TPostgreSQLUniProvider;
    uniSQL: TUniSQL;
    clSmtp: TclSmtp;
    clMailMessage: TclMailMessage;
    sDBGrid1: TsDBGrid;
    qryEpedido: TUniQuery;
    dsEpedido: TUniDataSource;
    qryItens: TUniQuery;
    dsItens: TDataSource;
    mmo1: TMemo;
    sdbgrdItems: TsDBGrid;
    pnlTop: TPanel;
    btnImportar: TButton;
    btnImportarCliente: TButton;
    btn_MergeEstados: TButton;
    btn_MergeMunicipios: TButton;
    btn_MergeEnderecos: TButton;
    btn_MergeProdutos: TButton;
    btn_MergeTransportadoras: TButton;
    btn_MergePedidos: TButton;
    btn_MergeItensPedido: TButton;
    sbtbtn1: TsBitBtn;
    btnConfiguracoes: TButton;
    unsqlVendedor: TUniSQL;
    tmrBuscaDados: TTimer;
    btnBuildMail: TButton;
    sSkinManager1: TsSkinManager;
    btnTemplate: TButton;
    qryEpedidoeped_cod_cliente: TStringField;
    qryEpedidonome_cliente: TStringField;
    qryEpedidoeped_codigo: TStringField;
    qryEpedidoeped_emissao: TDateField;
    qryEpedidoeped_entrega: TDateField;
    qryEpedidoeped_frete_resp: TStringField;
    qryEpedidoeped_pagamento: TStringField;
    qryEpedidotrans_nome: TStringField;
    qryEpedidotrans_telefone: TStringField;
    qryEpedidotrans_email: TStringField;
    qryEpedidoeped_vendedor: TStringField;
    qryEpedidoemail_vend: TStringField;
    qryEpedidoeped_contato_cliente: TStringField;
    qryEpedidoeped_email_contato: TStringField;
    qryEpedidomun_municipio: TStringField;
    qryEpedidoest_uf: TStringField;
    qryEpedidoend_endereco: TStringField;
    qryEpedidoend_bairro: TStringField;
    qryEpedidoend_cep: TStringField;
    qryEpedidoeped_cancelado: TStringField;
    qryEpedidoeped_analise: TStringField;
    qryEpedidoeped_faturado: TStringField;
    qryEpedidoeped_parcial: TStringField;
    qryEpedidoeped_pendente: TStringField;
    qryEpedidoeped_cancelado_send: TStringField;
    qryEpedidoeped_analise_send: TStringField;
    qryEpedidoeped_faturado_send: TStringField;
    qryEpedidoeped_parcial_send: TStringField;
    qryEpedidoeped_pendente_send: TStringField;
    qryEpedidoeped_valor_total: TFloatField;
    qryEpedidoeped_valor_st: TFloatField;
    qryEpedidoeped_aliq_st: TFloatField;
    qryEpedidoeped_valor_icms: TFloatField;
    qryEpedidoeped_aliq_icms: TFloatField;
    qryEpedidoeped_valor_ipi: TFloatField;
    qryEpedidoeped_aliq_ipi: TFloatField;
    qryEpedidoeped_valor_itens: TFloatField;
    qryEpedidoeped_filial: TStringField;
    strngfldEpedidotrans_codigo: TStringField;
    ApplicationEvents1: TApplicationEvents;
    dbgrdStatus: TDBGrid;
    strngfldEpedidosit_descricao: TStringField;
    connSrv: TUniConnection;
    tbPedido: TUniTable;
    tbPedidoped_cod_cliente: TStringField;
    tbPedidoped_nome_cliente: TStringField;
    tbPedidoped_contato_cliente: TStringField;
    tbPedidoped_data_emissao: TDateField;
    tbPedidoped_data_entrega: TDateField;
    tbPedidoped_nome_codpagamento: TStringField;
    tbPedidoped_end_entrega: TStringField;
    tbPedidoped_cep_entrega: TStringField;
    tbPedidoped_bairro_entrega: TStringField;
    tbPedidoped_municipio_entrega: TStringField;
    tbPedidoped_uf_entrega: TStringField;
    tbPedidoped_num_pedido: TStringField;
    tbPedidoped_situacao: TStringField;
    tbPedidoped_cod_transp: TStringField;
    tbPedidoped_nome_transp: TStringField;
    tbPedidoped_frete_resp: TStringField;
    tbPedidoped_cod_prod: TStringField;
    tbPedidoped_nome_prod: TStringField;
    tbPedidoped_qnt_solicitada: TFloatField;
    tbPedidoped_qnt_liberada: TFloatField;
    tbPedidoped_qnt_faturada: TFloatField;
    tbPedidoped_qnt_pendente: TFloatField;
    tbPedidoped_valor_unit: TFloatField;
    tbPedidoped_valor_total: TFloatField;
    tbPedidoped_valor_ttl_ipi: TFloatField;
    tbPedidoped_valor_liq_ttl: TFloatField;
    tbPedidoped_aliq_ipi: TFloatField;
    tbPedidoped_valor_ipi: TFloatField;
    tbPedidoped_aliq_icms: TFloatField;
    tbPedidoped_valor_icms: TFloatField;
    tbPedidoped_aliq_icmsst: TFloatField;
    tbPedidoped_valor_icmsst: TFloatField;
    tbPedidoped_qnt_volumes: TFloatField;
    tbPedidoped_peso_bruto: TFloatField;
    tbPedidoped_peso_liquido: TFloatField;
    tbPedidoped_qnt_ttl_volumes: TFloatField;
    tbPedidoped_peso_ttlbruto: TFloatField;
    tbPedidoped_peso_ttlliquido: TFloatField;
    tbPedidoped_formapagamento: TStringField;
    tbPedidoped_data_vencimento: TDateField;
    tbPedidoped_valor_parcela: TFloatField;
    tbPedidoped_num_parcela: TFloatField;
    tbPedidoped_email_cliente: TStringField;
    tbPedidoped_contato_cliente2: TStringField;
    tbPedidoped_email_contato: TStringField;
    tbPedidoped_cod_vendedor: TStringField;
    tbPedidoped_filial: TStringField;
    pgBarMail: TsProgressBar;
    procedure btnImportarClienteClick(Sender: TObject);
    procedure btn_MergeEstadosClick(Sender: TObject);
    procedure btn_MergeMunicipiosClick(Sender: TObject);
    procedure btn_MergeEnderecosClick(Sender: TObject);
    procedure btn_MergeProdutosClick(Sender: TObject);
    procedure btn_MergeTransportadorasClick(Sender: TObject);
    procedure btn_MergePedidosClick(Sender: TObject);
    procedure btn_MergeItensPedidoClick(Sender: TObject);
    procedure QImport3ASCII1AfterPost(Sender: TObject; Row: TQImportRow);
    procedure QImport3ASCII1AfterImport(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnConfiguracoesClick(Sender: TObject);
    procedure sbtbtn1Click(Sender: TObject);
    procedure tmrBuscaDadosTimer(Sender: TObject);
    procedure btnImportarClick(Sender: TObject);
    procedure btnBuildMailClick(Sender: TObject);
    procedure btnTemplateClick(Sender: TObject);
    procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
    procedure clSmtpProgress(Sender: TObject; ABytesProceed,
      ATotalBytes: Int64);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;
  servidor, porta, usuario, senha, PATHCLIENTE, PATHTRANSPORTADORA, LOG: string;
  dbserver, dbport, dbuser, dbsenha, dbDatabase, dbProvider: string;
  EmailCopia: string;
  PATHVENDAS: string;
  TempoEnvio: integer;
  habilitado, MailSSL: Boolean;
  _boolEnviando: Boolean;
  emissaoini: TDate;

implementation

{$R *.dfm}

uses
  uConfiguracoes, uSecurity, uTemplate, uFuncoes;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  fConfig: TIniFile;
begin
  NullStrictConvert := false;
  fConfig := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  with fConfig do
  begin
    servidor := ReadString('Email', 'Servidor', '');
    porta := ReadString('Email', 'Porta', '');
    usuario := ReadString('Email', 'Usuario', '');
    MailSSL := ReadBool('Email', 'AutenticacaoTSLSSL', true);
    senha := ReadString('Email', 'Senha', '');
    EmailCopia := ReadString('Email', 'EMailCopia', '');
    PATHCLIENTE := ReadString('Arquivo', 'Clientes', '');
    PATHTRANSPORTADORA := ReadString('Arquivo', 'Transportadoras', '');
    LOG := ReadString('Arquivo', 'Log', '');
    PATHVENDAS := ReadString('Arquivo', 'Vendas', '');
    TempoEnvio := ReadInteger('Tempo', 'TempoEnvio', 1);
    habilitado := ReadBool('Tempo', 'Habilitado', false);
    dbserver := ReadString('Database', 'server', '');
    dbport := ReadString('Database', 'port', '');
    dbuser := ReadString('Database', 'user', '');
    dbsenha := ReadString('Database', 'senha', '');
    dbDatabase := ReadString('Database', 'Database', '');
    dbProvider := ReadString('Database', 'Provider', '');
    emissaoini := ReadDate('Emissao', 'EmissaoPedido', now);
    qryEpedido.ParamByName('emissao').AsDate := emissaoini;
  end;
  with connSrv do
  begin
     {Configura a conexa com o Banco de Dados}
    Server := dbserver;
    Port := StrToInt(dbPort);
    Username := dbuser;
    Password := dbsenha;
    Database := dbDatabase;
    ProviderName := dbProvider;
    try
      Connect
    except
      on E: exception do
        raise Exception.Create('N�o poss�vel conectar ao banco de dados');
    end;
    qryEpedido.Open;
    qryItens.Open;
  end;
end;

procedure TfrmMain.QImport3ASCII1AfterImport(Sender: TObject);
begin
  sProgressBar1.Position := 0;
  Updates_Ajustes.Execute;
  btn_MergeEstados.Click;
  btn_MergeMunicipios.Click;
  btn_MergeEnderecos.Click;
  btn_MergeProdutos.Click;
  btnImportarCliente.Click;
  btn_MergeTransportadoras.Click;
  btn_MergePedidos.Click;
  btn_MergeItensPedido.Click;
  qryEpedido.Refresh;
end;

procedure TfrmMain.QImport3ASCII1AfterPost(Sender: TObject; Row: TQImportRow);
begin
  sProgressBar1.StepBy(1);
end;

procedure TfrmMain.sbtbtn1Click(Sender: TObject);
var
  retV: uFuncoes.TStringArray;
  fVendedor: TextFile;
  strLinFVendedor: string;
  sqlInsert: string;
  func: TFuncoes;
begin
  func := TFuncoes.Create;

  try
    AssignFile(fVendedor, 'c:\EmailTransacional\exportadados\vendedores.txt');
    Reset(fVendedor);
  except
    on EInOutError do
      exit;
  end;

  while not EOF(fVendedor) do
  begin
    ReadLn(fVendedor, strLinFVendedor);
    retV := func.Split(strLinFVendedor, '|', 0);
    retV[0] := StringReplace(retV[0], '"', '', [rfReplaceall]);
    retV[1] := StringReplace(retV[1], '"', '', [rfReplaceall]);
    sqlInsert := 'insert into tbvendedor(codigo_vend, nome_vend,email_vend) values(:COD,:NOME,:Email)';
    unsqlVendedor.SQL.Add(sqlInsert);
    unsqlVendedor.Params.Items[0].AsString := retV[0];
    unsqlVendedor.Params.Items[1].AsString := retV[1];
    unsqlVendedor.Params.Items[2].AsString := 'daniel.caria@yahoo.com.br';
    unsqlVendedor.Execute;
    unsqlVendedor.SQL.Clear;
  end;
  func.Free;
end;

procedure TfrmMain.ApplicationEvents1Exception(Sender: TObject; E: Exception);
var
  NomeDoLog: string;
  Arquivo: TextFile;
begin
    //NomeDoLog := ChangeFileExt(Application.Exename, '.log');
  NomeDoLog := LOG;
  AssignFile(Arquivo, NomeDoLog);

  if FileExists(NomeDoLog) then
    Append(arquivo) { se existir, apenas adiciona linhas }
  else
    ReWrite(arquivo); { cria um novo se n�o existir }
  try
    WriteLn(arquivo, DateTimeToStr(Now) + ':' + E.Message);
    WriteLn(arquivo, '------------------------------------------------------------------------------------------------------------------------------------');
      //Application.ShowException(E);
  finally
    CloseFile(arquivo)
  end;
end;

procedure TfrmMain.btnBuildMailClick(Sender: TObject);
var
  TMail: TBuildMail;
  I: integer;
  HTMLMail: TStringList;
  P, Q, R: array of string;
  NumPedido: string;
  strAssunto: string;
begin
  if qryEpedido.IsEmpty then
  begin
    Exit;
  end;
  _boolEnviando := true;

  // Atualiza a tabela de Pedidos
  qryEpedido.Refresh;

  with qryEpedido do
  begin
    while not qryEpedido.EOF do
    begin
      NumPedido := FieldByName('eped_codigo').AsString;
      HTMLMail := TStringList.Create;

      with qryItens do
      begin
        I := 0;
        setlength(P, qryItens.RecordCount);
        setlength(Q, qryItens.RecordCount);
        setlength(R, qryItens.RecordCount);
        while not EOF do
        begin
          P[I] := FieldByName('epedi_cod_produto').AsString;
          Q[I] := FieldByName('epedi_qnt_solicitada').AsString;
          R[I] := FieldByName('prod_descricao').AsString;
          inc(I);
          Next;
        end;
      end;

      TMail := TBuildMail.Create;
      TMail.SearchOK := false;

      // Qual a condi��o do status - Verificar no Banco de Dados importado do Radar
      // E atribuir a imagem conforme o status;

      // Campos da condi��o do Banco de Dados, o status ser� alterado Aqui.

      with qryEpedido do
      begin
        // Pedido Cancelado
        if (FieldByName('eped_cancelado').Value = 'S') and VarIsNull(FieldByName('eped_cancelado_send').Value) then
        begin
          TMail.ImageStatus := 'http://i1098.photobucket.com/albums/g372/danielcaria1/cancelado_zpsxtnqduwy.jpg';
          TMail.Status := 'Cancelado';
          TMail.Field := 'eped_cancelado_send';
          TMail.SearchOK := true;
          strAssunto := 'Pedido #' + NumPedido + ' Cancelado';
        end;
        // Pedido Realizado
        if (FieldByName('eped_analise').Value = 'S') and VarIsNull(FieldByName('eped_analise_send').Value) then
        begin
          TMail.ImageStatus := 'http://i1074.photobucket.com/albums/w414/DanielCaria/bullfor_status_realizado_zps5ezqrewh.png';
          TMail.Status := 'Realizado';
          TMail.Field := 'eped_analise_send';
          TMail.SearchOK := true;
          strAssunto := 'Pedido #' + NumPedido + ' Realizado';
        end;
        // Pedido Aprovado
        if (FieldByName('eped_pendente').Value = 'S') and VarIsNull(FieldByName('eped_pendente_send').Value) then
        begin
          TMail.ImageStatus := 'http://i1074.photobucket.com/albums/w414/DanielCaria/bullfor_status_aprovado_zpseiujffnf.png';
          TMail.Status := 'Aprovado';
          TMail.Field := 'eped_pendente_send';
          TMail.SearchOK := true;
          strAssunto := 'Pedido #' + NumPedido + ' Aprovado';
        end;
        // Pedido Faturado
        if (FieldByName('eped_faturado').Value = 'S') and VarIsNull(FieldByName('eped_faturado_send').Value) then
        begin
          TMail.ImageStatus := 'http://i1074.photobucket.com/albums/w414/DanielCaria/bullfor_status_faturado_zpslq2dksyh.png';
          TMail.Status := 'Faturado';
          TMail.Field := 'eped_faturado_send';
          TMail.SearchOK := true;
          strAssunto := 'Pedido #' + NumPedido + ' Faturado';
        end;
      end;
      TMail.Filial := FieldByName('eped_filial').AsString;
      // Informa��es do Contato - Cliente
      TMail.Pedido := FieldByName('eped_codigo').AsString;
      TMail.Contato := FieldByName('eped_contato_cliente').AsString;
      TMail.Empresa := FieldByName('nome_cliente').AsString;
      TMail.Endereco := FieldByName('end_endereco').AsString;
      TMail.Municipio := FieldByName('mun_municipio').AsString;
      TMail.UF := FieldByName('est_uf').AsString;
      TMail.CEP := FieldByName('end_cep').AsString;
      TMail.Bairro := FieldByName('end_bairro').AsString;
      TMail.EmailContato := FieldByName('eped_email_contato').AsString;

      if TMail.EmailContato = EmailCopia then
      begin
        strAssunto := 'Pedido sem email no Contato';
      end;

      // Informa��es da Transportadora
      TMail.TranspCodigo := FieldByName('trans_codigo').AsString;
      TMail.TransNome := FieldByName('trans_nome').AsString;
      TMail.TransTelefone := FieldByName('trans_telefone').AsString;
      TMail.TransEmail := FieldByName('trans_email').AsString;

      // Informa��es da Forma de Pagamento
      TMail.Pagamento := FieldByName('eped_pagamento').AsString;

      // Informa��es dos Valores
      TMail.ValorItens := Format('%n', [FieldByName('eped_valor_itens').AsFloat]);
      TMail.ValorIPI := Format('%n', [FieldByName('eped_valor_ipi').AsFloat]);
      TMail.ValorICMS := Format('%n', [FieldByName('eped_valor_icms').AsFloat]);
      TMail.ValorST := Format('%n', [FieldByName('eped_valor_st').AsFloat]);
      TMail.ValorTotal := Format('%n', [FieldByName('eped_valor_total').AsFloat + FieldByName('eped_valor_st').AsFloat]);

      // Chama a fun��o que Cria o e-mail e anexa no servi�o de SMTP.
      HTMLMail := TMail.BuldMail(P, Q, R);

      if TMail.SearchOK then
      begin
        {Configura servidor de e-mail}
        clSmtp.Server := servidor;
        clSmtp.Port := strToint(porta);
        clSmtp.UserName := usuario;
        clSmtp.Password := senha;
        {Verifica se utiliza conexao segura TLS}
        if MailSSL then
        begin
          clSmtp.UseTLS := ctAutomatic;
        end
        else
        begin
          clSmtp.UseTLS := ctNone;
        end;

        // Abre a conex�o de e-mail
        try
          clSmtp.Open();
        except
          on E: exception do
            raise Exception.Create('N�o Foi poss�vel conectar ao servidor de Email ' + E.Message);
        end;

        {Informa��es do E-mail - Corpo e Destinat�rios}
        clMailMessage.BuildMessage('', HTMLMail.GetText);
        clMailMessage.From.FullAddress := usuario;
        clMailMessage.From.Name := 'Bullfor';
        clMailMessage.ToList.EmailAddresses := TMail.EmailContato;
          if TMail.EmailContato <> EmailCopia then
          begin
              clMailMessage.BCCList.Add.Email := EmailCopia;
          end;
        clMailMessage.BCCList.Add.Email := 'daniel.caria@bol.com.br';
        clMailMessage.Subject := strAssunto;

        {Tenta enviar e-mail - Se n�o Conseguir sai da Rotina}
        try
          clSmtp.Send(clMailMessage);
          qryEpedido.Edit;
          qryEpedido.FieldByName(TMail.Field).AsString := 'S';
          qryEpedido.Post;
          pgBarMail.Position := 0;
        finally
          HTMLMail.Free;
          TMail.Free;
        end;
      end;
      qryEpedido.Next;
    end;
  end;

  clSmtp.Close();
  qryEpedido.Close;
  qryEpedido.Open;
  qryEpedido.First;
  _boolEnviando := false;
end;

procedure TfrmMain.btnConfiguracoesClick(Sender: TObject);
begin
  Application.CreateForm(TfrmConfiguracoes, frmConfiguracoes);
  frmConfiguracoes.show;
end;

procedure TfrmMain.btnImportarClick(Sender: TObject);
var
  tLista, tImporta: TextFile;
  lin, strLinha, AntProd, AntNomeProd, newName: string;
  x: integer;
  Ret: uFuncoes.TStringArray;
  I, prog: integer;
  TotLin: extended;
  func: TFuncoes;
begin
  func := TFuncoes.Create;
    //Se o arquivo de vendas n�o existe gera uma exce��o e sai da rotina
  if not FileExists(PATHVENDAS) then
  begin
    exit;
  end;

    // Timer Busca Dados, desabilita a fun��o TIMER.
  tmrBuscaDados.Enabled := false;

  tbPedido.Open;

  TotLin := func.FastLinesCount(PATHVENDAS);
  sProgressBar1.Max := trunc(TotLin);
  prog := 0;
  x := 1;
  // Tenta abrir arquivo, se n�o encontrar sai da rotina.
  try
    AssignFile(tLista, PATHVENDAS);
    AssignFile(tImporta, 'c:\EmailTransacional\importa.txt');
    Reset(tLista);
    rewrite(tImporta);
  except
    on E: Exception do
      raise Exception.Create('Erro no arquivo de importa��o de pedidos: ' + e.Message);
  end;

  while not EOF(tLista) do
  begin
    ReadLn(tLista, lin);
    lin := StringReplace(lin, '"', '', [rfReplaceall]);
    Ret := func.Split(lin, '|', 0);
    for I := 0 to Length(Ret) - 1 do
    begin
      StrReplace(Ret[17], '""', '"', [rfReplaceall]);
      if Length(Ret[16]) = 2 then
      begin
        Ret[16] := AntProd;
        Ret[17] := AntNomeProd;
      end;
      strLinha := strLinha + Ret[I] + '|';
      AntProd := Ret[16];
      AntNomeProd := Ret[17];
    end;
    try
      if Length(ret[5]) = 0 then
      begin
            //func.Log('Error Message: Pedido ' + ret[11] + ' Sem forma de pagamento.',LOG);
      end;
    finally
      writeln(tImporta, strLinha);
      inc(prog);
      sProgressBar1.StepBy(prog);
      inc(x);
      strLinha := '';
    end;

  end;
  if prog = trunc(TotLin) then
  begin
    sProgressBar1.Position := 0;
  end;

  with uniSQL do
  begin
    SQL.Clear;
    SQL.Add('delete from tbpedido');
    Execute;
  end;

  newName := 'C:\EmailTransacional\logs\' + 'Pedido - ' + func.ReturnDataHora + '.txt';

  CloseFile(tLista);

  MoveFile(PChar(PATHVENDAS), PChar(newName));

  CloseFile(tImporta);

  QImport3ASCII1.Execute;

  tmrBuscaDados.Enabled := true;
  func.Free;
end;

procedure TfrmMain.btnImportarClienteClick(Sender: TObject);
begin
  with uniSQL do
  begin
    SQL.Clear;
    SQL.Add('select merge_cliente()');
    Execute;
  end;
end;

procedure TfrmMain.btnTemplateClick(Sender: TObject);
var
  TMail: TBuildMail;
  I: integer;
  HTMLMail: TStringList;
  P, Q, R: array of string;
  NumPedido: string;
  strAssunto: string;
begin
  with qryEpedido do
  begin
    begin
      NumPedido := FieldByName('eped_codigo').AsString;
      HTMLMail := TStringList.Create;

      with qryItens do
      begin
        I := 0;
        setlength(P, qryItens.RecordCount);
        setlength(Q, qryItens.RecordCount);
        setlength(R, qryItens.RecordCount);
        while not EOF do
        begin
          P[I] := FieldByName('epedi_cod_produto').AsString;
          Q[I] := FieldByName('epedi_qnt_solicitada').AsString;
          R[I] := FieldByName('prod_descricao').AsString;
          inc(I);
          Next;
        end;
      end;

      TMail := TBuildMail.Create;
      TMail.SearchOK := false;

      // Qual a condi��o do status - Verificar no Banco de Dados importado do Radar
      // E atribuir a imagem conforme o status;

      // Campos da condi��o do Banco de Dados, o status ser� alterado Aqui.

      with qryEpedido do
      begin
        // Pedido Cancelado
        if (FieldByName('eped_cancelado').Value = 'S') and VarIsNull(FieldByName('eped_cancelado_send').Value) then
        begin
          TMail.ImageStatus := 'http://i1098.photobucket.com/albums/g372/danielcaria1/cancelado_zpsxtnqduwy.jpg';
          TMail.Status := 'Cancelado';
          TMail.Field := 'eped_cancelado_send';
          TMail.SearchOK := true;
          strAssunto := 'Pedido #' + NumPedido + ' Cancelado';
        end;
        // Pedido Realizado
        if (FieldByName('eped_analise').Value = 'S') and VarIsNull(FieldByName('eped_analise_send').Value) then
        begin
          TMail.ImageStatus := 'http://i1074.photobucket.com/albums/w414/DanielCaria/bullfor_status_realizado_zps5ezqrewh.png';
          TMail.Status := 'Realizado';
          TMail.Field := 'eped_analise_send';
          TMail.SearchOK := true;
          strAssunto := 'Pedido #' + NumPedido + ' Realizado';
        end;
        // Pedido Aprovado
        if (FieldByName('eped_pendente').Value = 'S') and VarIsNull(FieldByName('eped_pendente_send').Value) then
        begin
          TMail.ImageStatus := 'http://i1074.photobucket.com/albums/w414/DanielCaria/bullfor_status_aprovado_zpseiujffnf.png';
          TMail.Status := 'Aprovado';
          TMail.Field := 'eped_pendente_send';
          TMail.SearchOK := true;
          strAssunto := 'Pedido #' + NumPedido + ' Aprovado';
        end;
        // Pedido Faturado
        if (FieldByName('eped_faturado').Value = 'S') and VarIsNull(FieldByName('eped_faturado_send').Value) then
        begin
          TMail.ImageStatus := 'http://i1074.photobucket.com/albums/w414/DanielCaria/bullfor_status_faturado_zpslq2dksyh.png';
          TMail.Status := 'Faturado';
          TMail.Field := 'eped_faturado_send';
          TMail.SearchOK := true;
          strAssunto := 'Pedido #' + NumPedido + ' Faturado';
        end;
      end;

        // Informa��es do Contato - Cliente
      TMail.Pedido := FieldByName('eped_codigo').AsString;
      TMail.Contato := FieldByName('eped_contato_cliente').AsString;
      TMail.Empresa := FieldByName('nome_cliente').AsString;
      TMail.Endereco := FieldByName('end_endereco').AsString;
      TMail.Municipio := FieldByName('mun_municipio').AsString;
      TMail.UF := FieldByName('est_uf').AsString;
      TMail.CEP := FieldByName('end_cep').AsString;

        // Informa��es da Transportadora
      TMail.TransNome := FieldByName('trans_nome').AsString;
      TMail.TransTelefone := FieldByName('trans_telefone').AsString;
      TMail.TransEmail := FieldByName('trans_email').AsString;

        // Informa��es da Forma de Pagamento
      TMail.Pagamento := FieldByName('eped_pagamento').AsString;

        // Informa��es dos Valores
      TMail.ValorItens := Format('%n', [FieldByName('eped_valor_itens').AsFloat]);
      TMail.ValorIPI := Format('%n', [FieldByName('eped_valor_ipi').AsFloat]);
      TMail.ValorICMS := Format('%n', [FieldByName('eped_valor_icms').AsFloat]);
      TMail.ValorST := Format('%n', [FieldByName('eped_valor_st').AsFloat]);
      TMail.ValorTotal := Format('%n', [FieldByName('eped_valor_total').AsFloat + FieldByName('eped_valor_st').AsFloat]);

        // Atribui a Filial
      TMail.Filial := FieldByName('eped_filial').AsString;

        // Chama a fun��o que Cria o e-mail e anexa no servi�o de SMTP.
      HTMLMail := TMail.BuldMail(P, Q, R);
      mmo1.Lines.AddStrings(HTMLMail);
    end;
  end;
  HTMLMail.Free;
  TMail.Free;
  mmo1.SelectAll;
  mmo1.CopyToClipboard;
end;

procedure TfrmMain.btn_MergeEnderecosClick(Sender: TObject);
begin
  with uniSQL do
  begin
    SQL.Clear;
    SQL.Add('select merge_enderecos()');
    Execute;
  end;
end;

procedure TfrmMain.btn_MergeEstadosClick(Sender: TObject);
begin
  with uniSQL do
  begin
    SQL.Clear;
    SQL.Add('select merge_estados()');
    Execute;
  end;
end;

procedure TfrmMain.btn_MergeItensPedidoClick(Sender: TObject);
begin
  with uniSQL do
  begin
    SQL.Clear;
    SQL.Add('select merge_epedidoitem()');
    Execute;
  end;
end;

procedure TfrmMain.btn_MergeMunicipiosClick(Sender: TObject);
begin
  with uniSQL do
  begin
    SQL.Clear;
    SQL.Add('select merge_municipio()');
    Execute;
  end;
end;

procedure TfrmMain.btn_MergePedidosClick(Sender: TObject);
begin
  with uniSQL do
  begin
    SQL.Clear;
    SQL.Add('select merge_epedido()');
    Execute;
  end;
end;

procedure TfrmMain.btn_MergeProdutosClick(Sender: TObject);
begin
  with uniSQL do
  begin
    SQL.Clear;
    SQL.Add('select merge_produtos()');
    Execute;
  end;
end;

procedure TfrmMain.btn_MergeTransportadorasClick(Sender: TObject);
var
  fTrans: TextFile;
  rLin: string;
  transrec: uFuncoes.TStringArray;
  func: TFuncoes;
begin
  func := TFuncoes.Create;
  try
    AssignFile(fTrans, PATHTRANSPORTADORA);
    Reset(fTrans);
  except
    on E: Exception do
      exit;
  end;

  while not EOF(fTrans) do
  begin
    ReadLn(fTrans, rLin);
    rLin := StringReplace(rLin, '"', '', [rfReplaceall]);
    transrec := func.Split(rLin, '|', 0);
    with uniSQL do
    begin
      SQL.Clear;
      SQL.Add('select importa_transportadoras(' + QuotedStr(transrec[0]) + ',' + QuotedStr(transrec[1]) + ',' + QuotedStr(transrec[7]) + ',' + QuotedStr(transrec[10]) + ',' + QuotedStr(transrec[2]) + ',' + QuotedStr(transrec[3]) + ',' + QuotedStr(transrec[4]) + ',' + QuotedStr(transrec[5]) + ',' + QuotedStr(transrec[6]) + ',' + QuotedStr(transrec[8]) + ',' + QuotedStr(transrec[9]) + ')');
      Execute;
      SQL.Clear;
      SQL.Add('select merge_transportadoras()');
      Execute;
    end;
  end;
  CloseFile(fTrans);
  func.Free;
end;

procedure TfrmMain.clSmtpProgress(Sender: TObject; ABytesProceed,
  ATotalBytes: Int64);
begin
    pgBarMail.Max := ATotalBytes;
    pgBarMail.Position := ABytesProceed;
end;

procedure TfrmMain.tmrBuscaDadosTimer(Sender: TObject);
begin
  if (_boolEnviando = false) then
  begin
    btnImportarCliente.Click;
    Sleep(9);
    btnImportar.Click;
    Sleep(9);
    btnBuildMail.Click;
  end;
end;

end.

