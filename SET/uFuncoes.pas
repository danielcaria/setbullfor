unit uFuncoes;

interface
uses
Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB, DBAccess, Uni,ShellApi,
clMailMessage, clTcpClient, clMC, clSmtp,clEmailAddress;

type
  TStringArray = array of string;

type
  TFuncoes = class
    public
    function FastLinesCount(sfilename: string): extended;
    function onlyNumber(const Text: string): string;
    function ReturnDataHora : string;
    function Split(const Str: string; Delimiter: Char; Max: integer) : TStringArray;
    procedure FileSystemAction(action: Integer; fromDir, toDir: string; form : TForm);
    procedure Log(Const msg : string;const caminho : string);

  end;


implementation

{ TFuncoes }

procedure TFuncoes.FileSystemAction(action: Integer; fromDir, toDir: string; form : TForm);
var SHFileOp:TSHFileOpStruct;
begin
SHFileOp.wnd := form.handle;
SHFileOp.wFunc :=action;
SHFileOp.pFrom :=Pchar(fromDir +#0+#0);
SHFileOp.pTo :=Pchar(toDir +#0+#0);
SHFileOp.fFlags:=FOF_SILENT or FOF_NOCONFIRMATION;
SHFileOp.fAnyOperationsAborted:=false;
SHFileOp.hNameMappings :=NIL;
SHFileOp.lpszProgressTitle :=NIL;
SHFileOperation(SHFileOp);
end;


procedure TFuncoes.Log(const msg: string; const caminho :string);
  var
    NomeDoLog: string;
    Arquivo: TextFile;
  begin
    NomeDoLog := caminho;
    AssignFile(Arquivo, NomeDoLog);

    if FileExists(NomeDoLog) then
    Append(arquivo) { se existir, apenas adiciona linhas }
    else
      ReWrite(arquivo); { cria um novo se n�o existir }
    try
      WriteLn(arquivo, DateTimeToStr(Now) + ':' + msg);
      WriteLn(arquivo,'------------------------------------------------------------------------------------------------------------------------------------');
    finally
      CloseFile(arquivo)
    end;

  end;

function TFuncoes.ReturnDataHora: string;
var
Data, Hora : String;
begin
  Data := DateToStr(Now);
  Hora := TimeToStr(Now);
  Data := StringReplace(Data,'/','',[rfReplaceAll]);
  Hora := StringReplace(Hora,':','',[rfReplaceAll]);
  Data := Data + ' - ' + Hora;
  result := Data;
end;




function TFuncoes.Split(const Str: string; Delimiter: Char; Max: integer) : TStringArray;
var
  Size, I: integer;
begin
  Size := 0;
  setlength(result, 1);
  result[0] := '';
  for I := 1 to Length(Str) do
  begin
    if Str[I] = Delimiter then
    begin
      inc(Size);
      if (Max <> 0) and (Size = Max) then
        Break;
      setlength(result, Size + 1);
    end
    else
      result[Size] := result[Size] + Str[I];
  end;
end;

function TFuncoes.onlyNumber(const Text: string): string;
var
  strOnlyNumber: string;
  I: integer;
begin
  for I := 0 to Length(Text) - 1 do
  begin
    if (Text[I] in ['0' .. '9']) then
    begin
      strOnlyNumber := strOnlyNumber + Text[I];
    end;
  end;
  result := strOnlyNumber;
end;

function TFuncoes.FastLinesCount(sfilename: string): extended;
var
  hFile: TextFile;
  sLine: string;
  iLinescount: extended;
begin
  result := 0;
  if not FileExists(sfilename) then
    exit;

  AssignFile(hFile, sfilename);
  Reset(hFile);

  iLinescount := 0;
  while not EOF(hFile) do
  begin
    ReadLn(hFile, sLine);
    iLinescount := iLinescount + 1;
  end;
  CloseFile(hFile);
  result := iLinescount;
end;




end.
