program Split;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  uMain in 'uMain.pas' {frmMain},
  uConfiguracoes in 'uConfiguracoes.pas' {frmConfiguracoes},
  Vcl.Themes,
  Vcl.Styles,
  uTemplate in 'uTemplate.pas',
  uFuncoes in 'uFuncoes.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
