object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Importa'#231#227'o de Arquivos para Banco de Dados'
  ClientHeight = 549
  ClientWidth = 1010
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object sProgressBar1: TsProgressBar
    Left = 0
    Top = 0
    Width = 1010
    Height = 25
    Align = alTop
    TabOrder = 0
    SkinData.SkinSection = 'GAUGE'
  end
  object sDBGrid1: TsDBGrid
    Left = 0
    Top = 137
    Width = 1010
    Height = 162
    Align = alTop
    BorderStyle = bsNone
    DataSource = dsEpedido
    DrawingStyle = gdsGradient
    GradientEndColor = 15266803
    GradientStartColor = 15989241
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    SkinData.SkinSection = 'SCROLLBAR1H'
    Columns = <
      item
        Expanded = False
        FieldName = 'nome_cliente'
        Title.Caption = 'Cliente'
        Width = 257
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'eped_codigo'
        Title.Caption = 'C'#243'digo'
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'eped_emissao'
        Title.Caption = 'Emiss'#227'o'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'eped_entrega'
        Title.Caption = 'Entrega'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'eped_pagamento'
        Title.Caption = 'Cond. Pagamento'
        Width = 145
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'trans_nome'
        Title.Caption = 'Transportadora'
        Width = 280
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'eped_contato_cliente'
        Title.Caption = 'Contato'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'eped_email_contato'
        Title.Caption = 'e-mail - Contato'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'mun_municipio'
        Title.Caption = 'Cidade'
        Width = 127
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'est_uf'
        Title.Caption = 'Estado'
        Width = 35
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'end_endereco'
        Title.Caption = 'Endere'#231'o'
        Width = 233
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'end_bairro'
        Title.Caption = 'Bairro'
        Width = 106
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'end_cep'
        Title.Caption = 'CEP'
        Width = 64
        Visible = True
      end>
  end
  object mmo1: TMemo
    Left = 0
    Top = 379
    Width = 641
    Height = 170
    Align = alLeft
    BevelInner = bvLowered
    BorderStyle = bsNone
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 4
    WantTabs = True
    ExplicitTop = 347
    ExplicitHeight = 202
  end
  object sdbgrdItems: TsDBGrid
    Left = 0
    Top = 299
    Width = 1010
    Height = 80
    Align = alTop
    DataSource = dsItens
    DrawingStyle = gdsGradient
    Enabled = False
    GradientEndColor = 15266803
    GradientStartColor = 15989241
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ParentFont = False
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    SkinData.SkinSection = 'SPLITTER'
    SkinData.OuterEffects.Visibility = ovAlways
    Columns = <
      item
        Expanded = False
        FieldName = 'epedi_epedido'
        Title.Caption = 'C'#243'digo Pedido'
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'epedi_cod_produto'
        Title.Caption = 'Produto'
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'prod_descricao'
        Title.Caption = 'Descri'#231#227'o'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'epedi_qnt_solicitada'
        Title.Caption = 'Quantidade Solicitada'
        Width = 108
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'epedi_valor_unit'
        Title.Caption = 'Valor Unit'
        Width = 64
        Visible = True
      end>
  end
  object pnlTop: TPanel
    Left = 0
    Top = 25
    Width = 1010
    Height = 112
    Align = alTop
    TabOrder = 1
    object btnImportar: TButton
      Left = 8
      Top = 6
      Width = 130
      Height = 25
      Caption = 'Importar Pedidos'
      TabOrder = 0
      OnClick = btnImportarClick
    end
    object btnImportarCliente: TButton
      Left = 8
      Top = 37
      Width = 130
      Height = 25
      Caption = 'Importar Clientes'
      TabOrder = 7
      OnClick = btnImportarClienteClick
    end
    object btn_MergeEstados: TButton
      Left = 160
      Top = 6
      Width = 130
      Height = 25
      Caption = '01 - Estados'
      TabOrder = 1
      OnClick = btn_MergeEstadosClick
    end
    object btn_MergeMunicipios: TButton
      Left = 160
      Top = 37
      Width = 130
      Height = 25
      Caption = '02 - Municipios'
      TabOrder = 8
      OnClick = btn_MergeMunicipiosClick
    end
    object btn_MergeEnderecos: TButton
      Left = 304
      Top = 6
      Width = 130
      Height = 25
      Caption = '03 - Endere'#231'os'
      TabOrder = 2
      OnClick = btn_MergeEnderecosClick
    end
    object btn_MergeProdutos: TButton
      Left = 304
      Top = 37
      Width = 130
      Height = 25
      Caption = '04 - Produtos'
      TabOrder = 9
      OnClick = btn_MergeProdutosClick
    end
    object btn_MergeTransportadoras: TButton
      Left = 448
      Top = 6
      Width = 130
      Height = 25
      Caption = '05 - Transportadoras'
      TabOrder = 3
      OnClick = btn_MergeTransportadorasClick
    end
    object btn_MergePedidos: TButton
      Left = 448
      Top = 37
      Width = 130
      Height = 25
      Caption = '06 - Pedidos'
      TabOrder = 10
      OnClick = btn_MergePedidosClick
    end
    object btn_MergeItensPedido: TButton
      Left = 584
      Top = 6
      Width = 130
      Height = 25
      Caption = '07 - Itens do Pedido'
      TabOrder = 4
      OnClick = btn_MergeItensPedidoClick
    end
    object sbtbtn1: TsBitBtn
      Left = 584
      Top = 37
      Width = 130
      Height = 25
      Caption = '08 - Vendedores'
      TabOrder = 11
      OnClick = sbtbtn1Click
      Grayed = True
      SkinData.SkinSection = 'BUTTON'
    end
    object btnConfiguracoes: TButton
      Left = 731
      Top = 37
      Width = 121
      Height = 25
      Caption = 'Configura'#231#245'es'
      TabOrder = 12
      OnClick = btnConfiguracoesClick
    end
    object btnBuildMail: TButton
      Left = 731
      Top = 6
      Width = 121
      Height = 25
      Caption = 'Enviar e-mail'
      TabOrder = 5
      OnClick = btnBuildMailClick
    end
    object btnTemplate: TButton
      Left = 858
      Top = 6
      Width = 137
      Height = 25
      Caption = 'Template'
      TabOrder = 6
      OnClick = btnTemplateClick
    end
    object pgBarMail: TsProgressBar
      Left = 1
      Top = 94
      Width = 1008
      Height = 17
      Align = alBottom
      TabOrder = 13
      ExplicitLeft = 731
      ExplicitTop = 89
      ExplicitWidth = 264
    end
  end
  object dbgrdStatus: TDBGrid
    Left = 641
    Top = 379
    Width = 369
    Height = 170
    Align = alClient
    DataSource = dsEpedido
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'eped_codigo'
        Title.Caption = 'Pedido'
        Width = 102
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'sit_descricao'
        Title.Caption = 'Status'
        Visible = True
      end>
  end
  object QImport3ASCII1: TQImport3ASCII
    AutoTrimValue = True
    ImportEmptyRows = False
    DataSet = tbPedido
    Map.Strings = (
      'ped_cod_cliente=1'
      'ped_nome_cliente=2'
      'ped_contato_cliente=3'
      'ped_data_emissao=4'
      'ped_data_entrega=5'
      'ped_nome_codpagamento=6'
      'ped_end_entrega=7'
      'ped_cep_entrega=8'
      'ped_bairro_entrega=9'
      'ped_municipio_entrega=10'
      'ped_uf_entrega=11'
      'ped_num_pedido=12'
      'ped_situacao=13'
      'ped_cod_transp=14'
      'ped_nome_transp=15'
      'ped_frete_resp=16'
      'ped_cod_prod=17'
      'ped_nome_prod=18'
      'ped_qnt_solicitada=19'
      'ped_qnt_liberada=20'
      'ped_qnt_faturada=21'
      'ped_qnt_pendente=22'
      'ped_valor_unit=23'
      'ped_valor_total=24'
      'ped_valor_ttl_ipi=25'
      'ped_valor_liq_ttl=26'
      'ped_aliq_ipi=27'
      'ped_valor_ipi=28'
      'ped_aliq_icms=29'
      'ped_valor_icms=30'
      'ped_aliq_icmsst=31'
      'ped_valor_icmsst=32'
      'ped_qnt_volumes=33'
      'ped_peso_bruto=34'
      'ped_peso_liquido=35'
      'ped_qnt_ttl_volumes=36'
      'ped_peso_ttlbruto=37'
      'ped_peso_ttlliquido=38'
      'ped_formapagamento=39'
      'ped_data_vencimento=40'
      'ped_valor_parcela=41'
      'ped_num_parcela=42'
      'ped_contato_cliente2=43'
      'ped_email_contato=44'
      'ped_cod_vendedor=45'
      'ped_filial=48')
    Formats.DecimalSeparator = '.'
    Formats.ThousandSeparator = #0
    Formats.DateSeparator = '/'
    Formats.TimeSeparator = ':'
    Formats.BooleanTrue.Strings = (
      'True')
    Formats.BooleanFalse.Strings = (
      'False')
    Formats.NullValues.Strings = (
      'null')
    Formats.ShortDateFormat = 'dd/MM/yyyy'
    Formats.LongDateFormat = 'dddd, d'#39' de '#39'MMMM'#39' de '#39'yyyy'
    Formats.ShortTimeFormat = 'hh:mm'
    Formats.LongTimeFormat = 'hh:mm:ss'
    FieldFormats = <>
    ErrorLog = True
    ErrorLogFileName = 'error.log'
    CommitAfterDone = True
    AddType = qatInsert
    OnAfterImport = QImport3ASCII1AfterImport
    OnAfterPost = QImport3ASCII1AfterPost
    FileName = 'C:\EmailTransacional\importa.txt'
    Comma = '|'
    Quote = '"'
    Left = 606
    Top = 307
  end
  object UniTransaction1: TUniTransaction
    IsolationLevel = ilIsolated
    Left = 768
    Top = 469
  end
  object UniSQLMonitor1: TUniSQLMonitor
    Options = [moDialog, moSQLMonitor, moDBMonitor, moCustom, moHandled]
    DBMonitorOptions.Host = 'localhost'
    TraceFlags = [tfQPrepare, tfQExecute, tfQFetch, tfError, tfStmt, tfConnect, tfTransact, tfBlob, tfService, tfMisc, tfParams, tfObjDestroy, tfPool]
    Left = 606
    Top = 469
  end
  object Updates_Ajustes: TUniSQL
    Connection = connSrv
    SQL.Strings = (
      'select gerar_ajustes()')
    Left = 768
    Top = 388
  end
  object PostgreSQLUniProvider1: TPostgreSQLUniProvider
    Left = 930
    Top = 226
  end
  object uniSQL: TUniSQL
    Connection = connSrv
    Left = 849
    Top = 388
  end
  object clSmtp: TclSmtp
    CertificateFlags = [cfIgnoreCommonNameInvalid, cfIgnoreDateInvalid, cfIgnoreUnknownAuthority, cfIgnoreRevocation, cfIgnoreWrongUsage]
    TLSFlags = [tfUseSSL3, tfUseTLS]
    OnProgress = clSmtpProgress
    MailAgent = 'Clever Internet Suite'
    Left = 768
    Top = 226
  end
  object clMailMessage: TclMailMessage
    ToList = <>
    CCList = <>
    BCCList = <>
    Date = 42250.449370509260000000
    CharSet = 'iso-8859-1'
    ContentType = 'text/html'
    Left = 687
    Top = 226
  end
  object qryEpedido: TUniQuery
    Connection = connSrv
    SQL.Strings = (
      'select '
      'eped_cod_cliente,'
      'nome_cliente, '
      'eped_codigo, '
      'eped_emissao, '
      'eped_entrega, '
      'eped_frete_resp,'
      'eped_pagamento, '
      'tbtransportadora.trans_nome ,'
      'tbtransportadora.trans_telefone,'
      'tbtransportadora.trans_email, '
      'eped_vendedor, '
      'tbvendedor.email_vend,'
      'eped_contato_cliente, '
      'eped_email_contato,'
      'tbmunicipio.mun_municipio, '
      'tbestados.est_uf ,'
      'end_endereco,'
      'end_bairro,'
      'end_cep ,'
      'eped_cancelado,'
      'eped_analise,'
      'eped_faturado, '
      'eped_parcial ,'
      'eped_pendente,'
      'eped_cancelado_send,'
      'eped_analise_send,'
      'eped_faturado_send, '
      'eped_parcial_send,'
      'eped_pendente_send,'
      'eped_valor_total,'
      'eped_filial,'
      
        'eped_valor_st, eped_aliq_st, eped_valor_icms, eped_aliq_icms, ep' +
        'ed_valor_ipi, eped_aliq_ipi, eped_valor_itens,'
      'trans_codigo, sit_descricao'
      'from epedido'
      ' left outer join tbcliente'
      ' on eped_cod_cliente = tbcliente.id_cliente'
      
        ' left outer join tbenderecos on tbenderecos.end_id = epedido.epe' +
        'd_end_entrega'
      
        ' left outer join tbmunicipio on tbenderecos.end_municipio = tbmu' +
        'nicipio.mun_id'
      
        ' left outer join tbtransportadora on epedido.eped_transp = tbtra' +
        'nsportadora.trans_codigo'
      
        ' left outer join tbestados on tbmunicipio.mun_estado = tbestados' +
        '.est_id'
      
        ' left outer join tbvendedor on tbvendedor.codigo_vend = epedido.' +
        'eped_vendedor'
      ' inner join tbsituacao on sit_id = eped_situacao'
      
        'where eped_faturado_send is null and eped_cancelado_send is null' +
        ' and eped_emissao >= :emissao'
      'order by 3,2')
    Left = 687
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'emissao'
        Value = nil
      end>
    object qryEpedidoeped_cod_cliente: TStringField
      FieldName = 'eped_cod_cliente'
      Size = 30
    end
    object qryEpedidonome_cliente: TStringField
      FieldName = 'nome_cliente'
      ReadOnly = True
      Size = 255
    end
    object qryEpedidoeped_codigo: TStringField
      FieldName = 'eped_codigo'
      FixedChar = True
      Size = 12
    end
    object qryEpedidoeped_emissao: TDateField
      FieldName = 'eped_emissao'
    end
    object qryEpedidoeped_entrega: TDateField
      FieldName = 'eped_entrega'
    end
    object qryEpedidoeped_frete_resp: TStringField
      FieldName = 'eped_frete_resp'
      FixedChar = True
      Size = 30
    end
    object qryEpedidoeped_pagamento: TStringField
      FieldName = 'eped_pagamento'
      Size = 100
    end
    object qryEpedidotrans_nome: TStringField
      FieldName = 'trans_nome'
      ReadOnly = True
      Size = 255
    end
    object qryEpedidotrans_telefone: TStringField
      FieldName = 'trans_telefone'
      ReadOnly = True
      FixedChar = True
    end
    object qryEpedidotrans_email: TStringField
      FieldName = 'trans_email'
      ReadOnly = True
      Size = 255
    end
    object qryEpedidoeped_vendedor: TStringField
      FieldName = 'eped_vendedor'
      FixedChar = True
      Size = 10
    end
    object qryEpedidoemail_vend: TStringField
      FieldName = 'email_vend'
      ReadOnly = True
      Required = True
      Size = 200
    end
    object qryEpedidoeped_contato_cliente: TStringField
      FieldName = 'eped_contato_cliente'
      Size = 100
    end
    object qryEpedidoeped_email_contato: TStringField
      FieldName = 'eped_email_contato'
      Size = 255
    end
    object qryEpedidomun_municipio: TStringField
      FieldName = 'mun_municipio'
      ReadOnly = True
      Required = True
      Size = 120
    end
    object qryEpedidoest_uf: TStringField
      FieldName = 'est_uf'
      ReadOnly = True
      Required = True
      FixedChar = True
      Size = 2
    end
    object qryEpedidoend_endereco: TStringField
      FieldName = 'end_endereco'
      ReadOnly = True
      Required = True
      Size = 255
    end
    object qryEpedidoend_bairro: TStringField
      FieldName = 'end_bairro'
      ReadOnly = True
      Required = True
      Size = 255
    end
    object qryEpedidoend_cep: TStringField
      FieldName = 'end_cep'
      ReadOnly = True
      Required = True
      Size = 10
    end
    object qryEpedidoeped_cancelado: TStringField
      FieldName = 'eped_cancelado'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_analise: TStringField
      FieldName = 'eped_analise'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_faturado: TStringField
      FieldName = 'eped_faturado'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_parcial: TStringField
      FieldName = 'eped_parcial'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_pendente: TStringField
      FieldName = 'eped_pendente'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_cancelado_send: TStringField
      FieldName = 'eped_cancelado_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_analise_send: TStringField
      FieldName = 'eped_analise_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_faturado_send: TStringField
      FieldName = 'eped_faturado_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_parcial_send: TStringField
      FieldName = 'eped_parcial_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_pendente_send: TStringField
      FieldName = 'eped_pendente_send'
      FixedChar = True
      Size = 1
    end
    object qryEpedidoeped_valor_total: TFloatField
      FieldName = 'eped_valor_total'
    end
    object qryEpedidoeped_valor_st: TFloatField
      FieldName = 'eped_valor_st'
    end
    object qryEpedidoeped_aliq_st: TFloatField
      FieldName = 'eped_aliq_st'
    end
    object qryEpedidoeped_valor_icms: TFloatField
      FieldName = 'eped_valor_icms'
    end
    object qryEpedidoeped_aliq_icms: TFloatField
      FieldName = 'eped_aliq_icms'
    end
    object qryEpedidoeped_valor_ipi: TFloatField
      FieldName = 'eped_valor_ipi'
    end
    object qryEpedidoeped_aliq_ipi: TFloatField
      FieldName = 'eped_aliq_ipi'
    end
    object qryEpedidoeped_valor_itens: TFloatField
      FieldName = 'eped_valor_itens'
    end
    object qryEpedidoeped_filial: TStringField
      FieldName = 'eped_filial'
      FixedChar = True
      Size = 1
    end
    object strngfldEpedidotrans_codigo: TStringField
      FieldName = 'trans_codigo'
      ReadOnly = True
      FixedChar = True
      Size = 10
    end
    object strngfldEpedidosit_descricao: TStringField
      FieldName = 'sit_descricao'
      ReadOnly = True
      Size = 30
    end
  end
  object dsEpedido: TUniDataSource
    DataSet = qryEpedido
    Left = 930
    Top = 307
  end
  object qryItens: TUniQuery
    Connection = connSrv
    SQL.Strings = (
      
        'SELECT epedi_id, epedi_epedido, epedi_cod_produto,prod_descricao' +
        ', epedi_qnt_solicitada, '
      
        '       epedi_qnt_liberada, epedi_qnt_faturada, epedi_qnt_pendent' +
        'e, epedi_valor_unit, '
      
        '       epedi_valor_total, epedi_valor_ttl_ipi, epedi_valor_liq_t' +
        'tl, '
      
        '       epedi_aliq_ipi, epedi_valor_ipi, epedi_aliq_icms, epedi_v' +
        'alor_icms, '
      
        '       epedi_aliq_icmsst, epedi_valor_icmsst, epedi_qnt_volumes,' +
        ' epedi_peso_bruto, '
      
        '       epedi_peso_liquido, epedi_qnt_ttl_volumes, epedi_peso_ttl' +
        'bruto, '
      '       epedi_peso_ttlliquido'
      '  FROM epeditem'
      
        '  inner join tbproduto on epeditem.epedi_cod_produto = tbproduto' +
        '.prod_codigo'
      'where epedi_qnt_solicitada > 0'
      'order by epedi_qnt_solicitada')
    MasterSource = dsEpedido
    MasterFields = 'eped_codigo'
    DetailFields = 'epedi_epedido'
    Left = 606
    Top = 388
    ParamData = <
      item
        DataType = ftFixedChar
        Name = 'eped_codigo'
        ParamType = ptInput
        Value = '0416-000011'
      end>
  end
  object dsItens: TDataSource
    DataSet = qryItens
    Left = 849
    Top = 226
  end
  object unsqlVendedor: TUniSQL
    Connection = connSrv
    Left = 930
    Top = 388
  end
  object tmrBuscaDados: TTimer
    Interval = 900000
    OnTimer = tmrBuscaDadosTimer
    Left = 768
    Top = 307
  end
  object sSkinManager1: TsSkinManager
    Effects.AllowOuterEffects = True
    Effects.DiscoloredGlyphs = True
    ExtendedBorders = True
    AnimEffects.BlendOnMoving.Active = True
    ButtonsOptions.ShowFocusRect = False
    ButtonsOptions.ShiftContentOnClick = False
    Active = False
    InternalSkins = <>
    MenuSupport.IcoLineSkin = 'ICOLINE'
    MenuSupport.ExtraLineFont.Charset = DEFAULT_CHARSET
    MenuSupport.ExtraLineFont.Color = clWindowText
    MenuSupport.ExtraLineFont.Height = -11
    MenuSupport.ExtraLineFont.Name = 'Tahoma'
    MenuSupport.ExtraLineFont.Style = []
    Options.NativeBordersMaximized = True
    SkinDirectory = 'c:\Skins'
    SkinName = 'Zest'
    SkinInfo = 'N/A'
    ThirdParty.ThirdEdits = 
      'TEdit'#13#10'TMemo'#13#10'TMaskEdit'#13#10'TLabeledEdit'#13#10'THotKey'#13#10'TListBox'#13#10'TCheck' +
      'ListBox'#13#10'TRichEdit'#13#10'TDateTimePicker'#13#10'TSpinEdit'#13#10'TCategoryButtons' +
      #13#10'TFileListBox'#13#10'TDBListBox'#13#10'TDBMemo'#13#10'TDBLookupListBox'#13#10'TDBRichEd' +
      'it'#13#10'TDBCtrlGrid'#13#10'TDBEdit'#13#10'TfrxPreviewWorkspace'#13#10'TfrxScrollBox'#13#10'T' +
      'QRPreview'#13#10'TRVPrintPreview'#13#10'TSRVPageScroll'#13#10'TRVSpinEdit'#13#10
    ThirdParty.ThirdButtons = 'TButton'#13#10
    ThirdParty.ThirdBitBtns = 'TBitBtn'#13#10
    ThirdParty.ThirdCheckBoxes = 'TCheckBox'#13#10'TRadioButton'#13#10'TGroupButton'#13#10'TDBCheckBox'#13#10
    ThirdParty.ThirdGroupBoxes = 'TGroupBox'#13#10'TRadioGroup'#13#10'TDBRadioGroup'#13#10
    ThirdParty.ThirdListViews = 'TListView'#13#10
    ThirdParty.ThirdPanels = 
      'TPanel'#13#10'TPage'#13#10'TGridPanel'#13#10'TTabPage'#13#10'TDBNavigator'#13#10'TDBCtrlPanel'#13 +
      #10'TfrxTBPanel'#13#10
    ThirdParty.ThirdGrids = 
      'TStringGrid'#13#10'TDrawGrid'#13#10'TValueListEditor'#13#10'TDBGrid'#13#10'TRichView'#13#10'TD' +
      'BRichViewEdit'#13#10'TRichViewEdit'#13#10'TDBRichView'#13#10
    ThirdParty.ThirdTreeViews = 'TTreeView'#13#10'TDBTreeView'#13#10
    ThirdParty.ThirdComboBoxes = 'TComboBox'#13#10'TColorBox'#13#10'TComboBoxEx'#13#10'TDBComboBox'#13#10
    ThirdParty.ThirdWWEdits = ' '#13#10
    ThirdParty.ThirdVirtualTrees = ' '#13#10
    ThirdParty.ThirdGridEh = ' '#13#10
    ThirdParty.ThirdPageControl = 'TPageControl'#13#10
    ThirdParty.ThirdTabControl = 'TTabControl'#13#10'TTabbedNotebook'#13#10
    ThirdParty.ThirdToolBar = 'TToolBar'#13#10
    ThirdParty.ThirdStatusBar = 'TStatusBar'#13#10
    ThirdParty.ThirdSpeedButton = 'TSpeedButton'#13#10'TNavButton'#13#10
    ThirdParty.ThirdScrollControl = 'TScrollBox'#13#10
    ThirdParty.ThirdUpDown = 'TUpDown'#13#10'TSpinButton'#13#10
    ThirdParty.ThirdScrollBar = 'TScrollBar'#13#10
    ThirdParty.ThirdStaticText = 'TStaticText'#13#10
    ThirdParty.ThirdNativePaint = ' '#13#10
    Left = 687
    Top = 307
  end
  object ApplicationEvents1: TApplicationEvents
    OnException = ApplicationEvents1Exception
    Left = 606
    Top = 226
  end
  object connSrv: TUniConnection
    ProviderName = 'PostgreSQL'
    Left = 849
    Top = 307
  end
  object tbPedido: TUniTable
    TableName = 'tbpedido'
    Connection = connSrv
    Transaction = UniTransaction1
    Left = 687
    Top = 469
    object tbPedidoped_cod_cliente: TStringField
      FieldName = 'ped_cod_cliente'
      FixedChar = True
      Size = 30
    end
    object tbPedidoped_nome_cliente: TStringField
      FieldName = 'ped_nome_cliente'
      Size = 255
    end
    object tbPedidoped_contato_cliente: TStringField
      FieldName = 'ped_contato_cliente'
      Size = 255
    end
    object tbPedidoped_data_emissao: TDateField
      FieldName = 'ped_data_emissao'
    end
    object tbPedidoped_data_entrega: TDateField
      FieldName = 'ped_data_entrega'
    end
    object tbPedidoped_nome_codpagamento: TStringField
      FieldName = 'ped_nome_codpagamento'
      Size = 100
    end
    object tbPedidoped_end_entrega: TStringField
      FieldName = 'ped_end_entrega'
      Size = 255
    end
    object tbPedidoped_cep_entrega: TStringField
      FieldName = 'ped_cep_entrega'
      FixedChar = True
      Size = 12
    end
    object tbPedidoped_bairro_entrega: TStringField
      FieldName = 'ped_bairro_entrega'
      Size = 255
    end
    object tbPedidoped_municipio_entrega: TStringField
      FieldName = 'ped_municipio_entrega'
      Size = 255
    end
    object tbPedidoped_uf_entrega: TStringField
      FieldName = 'ped_uf_entrega'
      FixedChar = True
      Size = 2
    end
    object tbPedidoped_num_pedido: TStringField
      FieldName = 'ped_num_pedido'
      FixedChar = True
      Size = 12
    end
    object tbPedidoped_situacao: TStringField
      FieldName = 'ped_situacao'
      Size = 255
    end
    object tbPedidoped_cod_transp: TStringField
      FieldName = 'ped_cod_transp'
      Size = 10
    end
    object tbPedidoped_nome_transp: TStringField
      FieldName = 'ped_nome_transp'
      Size = 255
    end
    object tbPedidoped_frete_resp: TStringField
      FieldName = 'ped_frete_resp'
      Size = 100
    end
    object tbPedidoped_cod_prod: TStringField
      FieldName = 'ped_cod_prod'
      Size = 100
    end
    object tbPedidoped_nome_prod: TStringField
      FieldName = 'ped_nome_prod'
      Size = 255
    end
    object tbPedidoped_qnt_solicitada: TFloatField
      FieldName = 'ped_qnt_solicitada'
    end
    object tbPedidoped_qnt_liberada: TFloatField
      FieldName = 'ped_qnt_liberada'
    end
    object tbPedidoped_qnt_faturada: TFloatField
      FieldName = 'ped_qnt_faturada'
    end
    object tbPedidoped_qnt_pendente: TFloatField
      FieldName = 'ped_qnt_pendente'
    end
    object tbPedidoped_valor_unit: TFloatField
      FieldName = 'ped_valor_unit'
    end
    object tbPedidoped_valor_total: TFloatField
      FieldName = 'ped_valor_total'
    end
    object tbPedidoped_valor_ttl_ipi: TFloatField
      FieldName = 'ped_valor_ttl_ipi'
    end
    object tbPedidoped_valor_liq_ttl: TFloatField
      FieldName = 'ped_valor_liq_ttl'
    end
    object tbPedidoped_aliq_ipi: TFloatField
      FieldName = 'ped_aliq_ipi'
    end
    object tbPedidoped_valor_ipi: TFloatField
      FieldName = 'ped_valor_ipi'
    end
    object tbPedidoped_aliq_icms: TFloatField
      FieldName = 'ped_aliq_icms'
    end
    object tbPedidoped_valor_icms: TFloatField
      FieldName = 'ped_valor_icms'
    end
    object tbPedidoped_aliq_icmsst: TFloatField
      FieldName = 'ped_aliq_icmsst'
    end
    object tbPedidoped_valor_icmsst: TFloatField
      FieldName = 'ped_valor_icmsst'
    end
    object tbPedidoped_qnt_volumes: TFloatField
      FieldName = 'ped_qnt_volumes'
    end
    object tbPedidoped_peso_bruto: TFloatField
      FieldName = 'ped_peso_bruto'
    end
    object tbPedidoped_peso_liquido: TFloatField
      FieldName = 'ped_peso_liquido'
    end
    object tbPedidoped_qnt_ttl_volumes: TFloatField
      FieldName = 'ped_qnt_ttl_volumes'
    end
    object tbPedidoped_peso_ttlbruto: TFloatField
      FieldName = 'ped_peso_ttlbruto'
    end
    object tbPedidoped_peso_ttlliquido: TFloatField
      FieldName = 'ped_peso_ttlliquido'
    end
    object tbPedidoped_formapagamento: TStringField
      FieldName = 'ped_formapagamento'
      Size = 60
    end
    object tbPedidoped_data_vencimento: TDateField
      FieldName = 'ped_data_vencimento'
    end
    object tbPedidoped_valor_parcela: TFloatField
      FieldName = 'ped_valor_parcela'
    end
    object tbPedidoped_num_parcela: TFloatField
      FieldName = 'ped_num_parcela'
    end
    object tbPedidoped_email_cliente: TStringField
      FieldName = 'ped_email_cliente'
      Size = 255
    end
    object tbPedidoped_contato_cliente2: TStringField
      FieldName = 'ped_contato_cliente2'
      Size = 100
    end
    object tbPedidoped_email_contato: TStringField
      FieldName = 'ped_email_contato'
      Size = 255
    end
    object tbPedidoped_cod_vendedor: TStringField
      FieldName = 'ped_cod_vendedor'
      FixedChar = True
      Size = 10
    end
    object tbPedidoped_filial: TStringField
      FieldName = 'ped_filial'
      FixedChar = True
      Size = 1
    end
  end
end
