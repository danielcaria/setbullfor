unit uImportaPedido;

interface
uses System.SysUtils, System.Variants, System.Classes,
 Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,Winapi.Windows, Winapi.Messages,
 Uni, Data.Db, DBAccess;

type
  TStringArray = array of string;

  TImpPedido = class
    private
    FCodigoCliente: string;
    FNomeCliente: string;
    FContatoCliente: string;
    FDataEmissao: string;
    FDataEntrega: string;
    FNomePagamento: string;
    FEndEntrega: string;
    FCepEntrega: string;
    FBairroEntrega: string;
    FMunicipio: string;
    Fuf: string;
    FNumPedido: string;
    FSituacao: string;
    FCodTransp: string;
    FNomeTransp: string;
    FFreteResp: string;
    FCodProduto: string;
    FNomeProduto: string;
    FQntSolicitada: string;
    FQntLiberada: string;
    FQntFaturada: string;
    FQntPendente: string;
    FValorUnit: string;
    FValorTotal: string;
    FValorTotalIPI: string;
    FValorTotalLiq: string;
    FAliqIPI: string;
    FValorICMS: string;
    FAliqICMS: string;
    FValorIPI: string;
    FValorICMSST: string;
    FAliqICMSST: string;
    FQntVolumes: string;
    FPesoBruto: string;
    FPesoLiquido: string;
    FQntTotalVol: string;
    FPesoTotalBruto: string;
    FPesoTotalLiq: string;
    FFormaPagamento: string;
    FDataVencimento: string;
    FValorParcela: string;
    FNumParcela: string;
    FEmailCliente: string;
    FContaClienteEmail: string;
    FCodVendedor: string;
    FFilial: string;
    function Split(const Str: string; Delimiter: Char; Max: integer): TStringArray;
    procedure ImportaDados(Conexao : TUniConnection);
    procedure SetFCodigoCliente(const Value: string);
    procedure SetFNomeCliente(const Value: string);
    procedure SetFContatoCliente(const Value: string);
    procedure SetFBairroEntrega(const Value: string);
    procedure SetFCepEntrega(const Value: string);
    procedure SetFDataEmissao(const Value: string);
    procedure SetFDataEntrega(const Value: string);
    procedure SetFEndEntrega(const Value: string);
    procedure SetFNomePagamento(const Value: string);
    procedure SetFMunicipio(const Value: string);
    procedure setFuf(const Value: string);
    procedure setFNumPedido(const Value: string);
    procedure setFAliqICMS(const Value: string);
    procedure setFAliqICMSST(const Value: string);
    procedure setFAliqIPI(const Value: string);
    procedure setFCodProduto(const Value: string);
    procedure setFCodTransp(const Value: string);
    procedure setFCodVendedor(const Value: string);
    procedure setFContaClienteEmail(const Value: string);
    procedure setFDataVencimento(const Value: string);
    procedure setFEmailCliente(const Value: string);
    procedure setFFilial(const Value: string);
    procedure setFFormaPagamento(const Value: string);
    procedure setFFreteResp(const Value: string);
    procedure setFNomeProduto(const Value: string);
    procedure setFNomeTransp(const Value: string);
    procedure setFNumParcela(const Value: string);
    procedure setFPesoBruto(const Value: string);
    procedure setFPesoLiquido(const Value: string);
    procedure setFPesoTotalBruto(const Value: string);
    procedure setFPesoTotalLiq(const Value: string);
    procedure setFQntFaturada(const Value: string);
    procedure setFQntLiberada(const Value: string);
    procedure setFQntPendente(const Value: string);
    procedure setFQntSolicitada(const Value: string);
    procedure setFQntTotalVol(const Value: string);
    procedure setFQntVolumes(const Value: string);
    procedure setFSituacao(const Value: string);
    procedure setFValorICMS(const Value: string);
    procedure setFValorICMSST(const Value: string);
    procedure setFValorIPI(const Value: string);
    procedure setFValorParcela(const Value: string);
    procedure setFValorTotal(const Value: string);
    procedure setFValorTotalIPI(const Value: string);
    procedure setFValorTotalLiq(const Value: string);
    procedure setFValorUnit(const Value: string);
    function isDate(Date : string) : boolean;
    public
    property CodigoCliente: string read FCodigoCliente write SetFCodigoCliente;
    property NomeCliente: string read FNomeCliente write SetFNomeCliente;
    property ContatoCliente: string read FContatoCliente write SetFContatoCliente;
    property DataEmissao: string read FDataEmissao write SetFDataEmissao;
    property DataEntrega: string read FDataEntrega write SetFDataEntrega;
    property NomePagamento: string read FNomePagamento write SetFNomePagamento;
    property EndEntrega: string read FEndEntrega write SetFEndEntrega;
    property CepEntrega: string read FCepEntrega write SetFCepEntrega;
    property BairroEntrega: string read FBairroEntrega write SetFBairroEntrega;
    property Municipio: string read FMunicipio write SetFMunicipio;
    property uf: string read Fuf write setFuf;
    property NumPedido: string read FNumPedido write setFNumPedido;
    property Situacao: string read FSituacao write setFSituacao;
    property CodTransp: string read FCodTransp write setFCodTransp;
    property NomeTransp: string read FNomeTransp write setFNomeTransp;
    property FreteResp: string read FFreteResp write setFFreteResp;
    property CodProduto: string read FCodProduto write setFCodProduto;
    property NomeProduto: string read FNomeProduto write setFNomeProduto;
    property QntSolicitada: string read FQntSolicitada write setFQntSolicitada;
    property QntLiberada: string read FQntLiberada write setFQntLiberada;
    property QntFaturada: string read FQntFaturada write setFQntFaturada;
    property QntPendente: string read FQntPendente write setFQntPendente;
    property ValorUnit: string read FValorUnit write setFValorUnit;
    property ValorTotal: string read FValorTotal write setFValorTotal;
    property ValorTotalIPI: string read FValorTotalIPI write setFValorTotalIPI;
    property ValorTotalLiq: string read FValorTotalLiq write setFValorTotalLiq;
    property AliqIPI: string read FAliqIPI write setFAliqIPI;
    property ValorICMS: string read FValorICMS write setFValorICMS;
    property AliqICMS: string read FAliqICMS write setFAliqICMS;
    property ValorIPI: string read FValorIPI write setFValorIPI;
    property ValorICMSST: string read FValorICMSST write setFValorICMSST;
    property AliqICMSST: string read FAliqICMSST write setFAliqICMSST;
    property QntVolumes: string read FQntVolumes write setFQntVolumes;
    property PesoBruto: string read FPesoBruto write setFPesoBruto;
    property PesoLiquido: string read FPesoLiquido write setFPesoLiquido;
    property QntTotalVol: string read FQntTotalVol write setFQntTotalVol;
    property PesoTotalBruto: string read FPesoTotalBruto write setFPesoTotalBruto;
    property PesoTotalLiq: string read FPesoTotalLiq write setFPesoTotalLiq;
    property FormaPagamento: string read FFormaPagamento write setFFormaPagamento;
    property DataVencimento: string read FDataVencimento write setFDataVencimento;
    property ValorParcela: string read FValorParcela write setFValorParcela;
    property NumParcela: string read FNumParcela write setFNumParcela;
    property EmailCliente: string read FEmailCliente write setFEmailCliente;
    property ContaClienteEmail: string read FContaClienteEmail write setFContaClienteEmail;
    property CodVendedor: string read FCodVendedor write setFCodVendedor;
    property Filial: string read FFilial write setFFilial;
    procedure LerDados(const strfile: TextFile; path : string; Conexao: TUniConnection);
  end;
implementation

{ TImpPedido }

procedure TImpPedido.ImportaDados(Conexao: TUniConnection);
var
  Tabela : TUniSQL;
begin
      Tabela := TUniSQL.Create(nil);
      Tabela.Connection := Conexao;

      with tabela do begin
      SQL.Add('INSERT INTO tbpedido('+
            'ped_cod_cliente, ped_nome_cliente, ped_contato_cliente, ped_data_emissao,'+
            'ped_data_entrega, ped_nome_codpagamento, ped_end_entrega, ped_cep_entrega,'+
            'ped_bairro_entrega, ped_municipio_entrega, ped_uf_entrega, ped_num_pedido,'+
            'ped_situacao, ped_cod_transp, ped_nome_transp, ped_frete_resp,'+
            'ped_cod_prod, ped_nome_prod, ped_qnt_solicitada, ped_qnt_liberada,'+
            'ped_qnt_faturada, ped_qnt_pendente, ped_valor_unit, ped_valor_total,'+
            'ped_valor_ttl_ipi, ped_valor_liq_ttl, ped_aliq_ipi, ped_valor_ipi,'+
            'ped_aliq_icms, ped_valor_icms, ped_aliq_icmsst, ped_valor_icmsst,'+
            'ped_qnt_volumes, ped_peso_bruto, ped_peso_liquido, ped_qnt_ttl_volumes,'+
            'ped_peso_ttlbruto, ped_peso_ttlliquido, ped_formapagamento, ped_data_vencimento,'+
            'ped_valor_parcela, ped_num_parcela, ped_email_cliente, ped_contato_cliente2,'+
            'ped_email_contato, ped_cod_vendedor, ped_filial)'+
            'VALUES (:ped_cod_cliente,:ped_nome_cliente,:ped_contato_cliente,:ped_data_emissao,:ped_data_entrega,'+
            ':ped_nome_codpagamento,:ped_end_entrega, :ped_cep_entrega,:ped_bairro_entrega, :ped_municipio_entrega,'+
            ':ped_uf_entrega,:ped_num_pedido,:ped_situacao,:ped_cod_transp, :ped_nome_transp, :ped_frete_resp,:ped_cod_prod,'+
            ':ped_nome_prod, :ped_qnt_solicitada, :ped_qnt_liberada,:ped_qnt_faturada,:ped_qnt_pendente, :ped_valor_unit,'+
            ':ped_valor_total,:ped_valor_ttl_ipi, :ped_valor_liq_ttl, :ped_aliq_ipi, :ped_valor_ipi,:ped_aliq_icms,'+
            ':ped_valor_icms, :ped_aliq_icmsst, :ped_valor_icmsst,:ped_qnt_volumes, :ped_peso_bruto, :ped_peso_liquido,'+
            ':ped_qnt_ttl_volumes,:ped_peso_ttlbruto,:ped_peso_ttlliquido,:ped_formapagamento,:ped_data_vencimento,'+
            ':ped_valor_parcela,:ped_num_parcela,:ped_email_cliente,:ped_contato_cliente2,:ped_email_contato,:ped_cod_vendedor,'+
            ':ped_filial)');

                  ParamByName('ped_cod_cliente').value :=  CodigoCliente;
                  ParamByName('ped_nome_cliente').value :=  NomeCliente;
                  ParamByName('ped_contato_cliente').value := ContatoCliente;
                  if isDate(DataEmissao) then
                  begin
                  ParamByName('ped_data_emissao').value := DataEmissao;
                  end
                  else
                  begin
                  ParamByName('ped_data_emissao').Clear;
                  end;
                  if isDate(DataEntrega) then
                  begin
                  ParamByName('ped_data_entrega').value := DataEntrega;
                  end
                  else
                  begin
                  ParamByName('ped_data_entrega').Clear;
                  end;
                  ParamByName('ped_nome_codpagamento').value := NomePagamento;
                  ParamByName('ped_end_entrega').value := EndEntrega;
                  ParamByName('ped_cep_entrega').value := CepEntrega;
                  ParamByName('ped_bairro_entrega').value := BairroEntrega;
                  ParamByName('ped_municipio_entrega').value := Municipio;
                  ParamByName('ped_uf_entrega').value := uf;
                  ParamByName('ped_num_pedido').value := NumPedido;
                  ParamByName('ped_situacao').value := Situacao;
                  ParamByName('ped_cod_transp').value := CodTransp;
                  ParamByName('ped_nome_transp').value := NomeTransp;
                  ParamByName('ped_frete_resp').value := FreteResp;
                  ParamByName('ped_cod_prod').value := CodProduto;
                  ParamByName('ped_nome_prod').value := NomeProduto;
                  ParamByName('ped_qnt_solicitada').value := QntSolicitada;
                  ParamByName('ped_qnt_liberada').value := QntLiberada;
                  ParamByName('ped_qnt_faturada').value := QntFaturada;
                  ParamByName('ped_qnt_pendente').value := QntPendente;
                  ParamByName('ped_valor_unit').value := ValorUnit;
                  ParamByName('ped_valor_total').value := ValorTotal;
                  ParamByName('ped_valor_ttl_ipi').value := ValorTotalIPI;
                  ParamByName('ped_valor_liq_ttl').value := ValorTotalLiq;
                  ParamByName('ped_aliq_ipi').value := AliqIPI;
                  ParamByName('ped_valor_ipi').value := ValorIPI;
                  ParamByName('ped_aliq_icms').value := AliqICMS;
                  ParamByName('ped_valor_icms').value := ValorICMS;
                  ParamByName('ped_aliq_icmsst').value := AliqICMSST;
                  ParamByName('ped_valor_icmsst').value := ValorICMSST;
                  ParamByName('ped_qnt_volumes').value := QntVolumes;
                  ParamByName('ped_peso_bruto').value := PesoBruto;
                  ParamByName('ped_peso_liquido').value := PesoLiquido;
                  ParamByName('ped_qnt_ttl_volumes').value := QntTotalVol;
                  ParamByName('ped_peso_ttlbruto').value := PesoTotalBruto;
                  ParamByName('ped_peso_ttlliquido').value := PesoTotalLiq;
                  ParamByName('ped_formapagamento').value := FormaPagamento;
                  if isDate(DataVencimento) then
                  begin
                  ParamByName('ped_data_vencimento').value := DataVencimento;
                  end
                  else
                  begin
                  ParamByName('ped_data_vencimento').clear;
                  end;
                  ParamByName('ped_valor_parcela').value := ValorParcela;
                  ParamByName('ped_num_parcela').value := NumParcela;
                  ParamByName('ped_email_cliente').value := EmailCliente;
                  ParamByName('ped_contato_cliente2').value := ContatoCliente;
                  ParamByName('ped_email_contato').value := ContaClienteEmail;
                  ParamByName('ped_cod_vendedor').value := CodVendedor;
                  ParamByName('ped_filial').value := Filial;
       end;
      tabela.Execute;
      tabela.Free;
end;

function TImpPedido.isDate(Date: string): boolean;
Var
  dtValue : TDateTime;
Begin
        Try
                dtValue := StrToDateTime(Date);
                IsDate := True;
        Except
                On EConvertError Do IsDate := False;
        End;
end;

procedure TImpPedido.LerDados(const strfile: TextFile; path : string; Conexao: TUniConnection);
var
  strLine : String;
  mxStrImp : TStringArray;
  antCodProduto, antNomeProduto : String;
begin
      try
      Assign(strFile, path);
      reset(strFile);
      except
         on E : Exception do
         ShowMessage(E.ClassName + ' error raised, with message : ' + E.Message);
      end;

      while not eof(strfile) do
      begin
      Readln(strFile, strLine);
      strLine := StringReplace(strLine,'"','',[rfReplaceAll]);
      mxStrImp := Split(strLine,'|',0);
      {Atribui os valores para os campos}
      CodigoCliente   := mxStrImp[0];
      NomeCliente     := mxStrImp[1];
      ContatoCliente  := mxStrImp[2];
      DataEmissao     := mxStrImp[3];
      DataEntrega     := mxStrImp[4];
      NomePagamento   := mxStrImp[5];
      EndEntrega      := mxStrImp[6];
      CepEntrega      := mxStrImp[7];
      BairroEntrega   := mxStrImp[8];
      Municipio       := mxStrImp[9];
      uf              := mxStrImp[10];
      NumPedido       := mxStrImp[11];
      Situacao        := mxStrImp[12];
      CodTransp       := mxStrImp[13];
      NomeTransp      := mxStrImp[14];
      FreteResp       := mxStrImp[15];
      if Length(mxStrImp[16]) = 2 then
        begin
          mxStrImp[16] := antCodProduto;
          mxStrImp[17] := antNomeProduto;
        end;
      antCodProduto := mxStrImp[16];
      antNomeProduto := mxStrImp[17];

      CodProduto      := mxStrImp[16];
      NomeProduto     := mxStrImp[17];
      QntSolicitada   := mxStrImp[18];
      QntLiberada     := mxStrImp[19];
      QntFaturada     := mxStrImp[20];
      QntPendente     := mxStrImp[21];
      ValorUnit       := mxStrImp[22];
      ValorTotal      := mxStrImp[23];
      ValorTotalIPI   := mxStrImp[24];
      ValorTotalLiq   := mxStrImp[25];
      AliqIPI         := mxStrImp[26];
      ValorIPI        := mxStrImp[27];
      AliqICMS        := mxStrImp[28];
      ValorICMS       := mxStrImp[29];
      AliqICMSST      := mxStrImp[30];
      ValorICMSST     := mxStrImp[31];
      QntVolumes      := mxStrImp[32];
      PesoBruto       := mxStrImp[33];
      PesoLiquido     := mxStrImp[34];
      QntTotalVol     := mxStrImp[35];
      PesoTotalBruto  := mxStrImp[36];
      PesoTotalLiq    := mxStrImp[37];
      FormaPagamento  := mxStrImp[38];
      DataVencimento  := mxStrImp[39];
      ValorParcela    := mxStrImp[40];
      NumParcela      := mxStrImp[41];
      ContaClienteEmail := mxStrImp[42];
      EmailCliente    := mxStrImp[43];
      CodVendedor     := mxStrImp[44];
      Filial          := mxStrImp[47];
      ImportaDados(Conexao);
      end;
      Close(strfile);


end;

procedure TImpPedido.setFAliqICMS(const Value: string);
begin
  FAliqICMS := Value;
end;

procedure TImpPedido.setFAliqICMSST(const Value: string);
begin
  FAliqICMSST := Value;
end;

procedure TImpPedido.setFAliqIPI(const Value: string);
begin
  FAliqIPI := Value;
end;

procedure TImpPedido.SetFBairroEntrega(const Value: string);
begin
  FBairroEntrega := Value;
end;

procedure TImpPedido.SetFCepEntrega(const Value: string);
begin
  FCepEntrega := Value;
end;

procedure TImpPedido.SetFCodigoCliente(const Value: string);
begin
  FCodigoCliente := Value;
end;

procedure TImpPedido.setFCodProduto(const Value: string);
begin
  FCodProduto := Value;
end;

procedure TImpPedido.setFCodTransp(const Value: string);
begin
  FCodTransp := Value;
end;

procedure TImpPedido.setFCodVendedor(const Value: string);
begin
  FCodVendedor := Value;
end;

procedure TImpPedido.setFContaClienteEmail(const Value: string);
begin
  FContaClienteEmail := Value;
end;

procedure TImpPedido.SetFContatoCliente(const Value: string);
begin
  FContatoCliente := Value;
end;

procedure TImpPedido.SetFDataEmissao(const Value: string);
begin

    FDataEmissao := Value;
end;

procedure TImpPedido.SetFDataEntrega(const Value: string);
begin
  FDataEntrega := Value;
end;

procedure TImpPedido.setFDataVencimento(const Value: string);
begin

       FDataVencimento := Value;
end;

procedure TImpPedido.setFEmailCliente(const Value: string);
begin
  FEmailCliente := Value;
end;

procedure TImpPedido.SetFEndEntrega(const Value: string);
begin
  FEndEntrega := Value;
end;

procedure TImpPedido.setFFilial(const Value: string);
begin
  FFilial := Value;
end;

procedure TImpPedido.setFFormaPagamento(const Value: string);
begin
  FFormaPagamento := Value;
end;

procedure TImpPedido.setFFreteResp(const Value: string);
begin
  FFreteResp := Value;
end;

procedure TImpPedido.SetFMunicipio(const Value: string);
begin
  FMunicipio := Value;
end;

procedure TImpPedido.SetFNomeCliente(const Value: string);
begin
  FNomeCliente := Value;
end;

procedure TImpPedido.SetFNomePagamento(const Value: string);
begin
  if value.Length = 0 then
    begin
      ShowMessage('Erro encontrado');
    end;

  FNomePagamento := Value;
end;

procedure TImpPedido.setFNomeProduto(const Value: string);
begin
  FNomeProduto := Value;
end;

procedure TImpPedido.setFNomeTransp(const Value: string);
begin
  FNomeTransp := Value;
end;

procedure TImpPedido.setFNumParcela(const Value: string);
begin
  FNumParcela := Value;
end;

procedure TImpPedido.setFNumPedido(const Value: string);
begin
  FNumPedido := Value;
end;

procedure TImpPedido.setFPesoBruto(const Value: string);
begin
  FPesoBruto := Value;
end;

procedure TImpPedido.setFPesoLiquido(const Value: string);
begin
  FPesoLiquido := Value;
end;

procedure TImpPedido.setFPesoTotalBruto(const Value: string);
begin
  FPesoTotalBruto := Value;
end;

procedure TImpPedido.setFPesoTotalLiq(const Value: string);
begin
  FPesoTotalLiq := Value;
end;

procedure TImpPedido.setFQntFaturada(const Value: string);
begin
  FQntFaturada := Value;
end;

procedure TImpPedido.setFQntLiberada(const Value: string);
begin
  FQntLiberada := Value;
end;

procedure TImpPedido.setFQntPendente(const Value: string);
begin
  FQntPendente := Value;
end;

procedure TImpPedido.setFQntSolicitada(const Value: string);
begin
  FQntSolicitada := Value;
end;

procedure TImpPedido.setFQntTotalVol(const Value: string);
begin
  FQntTotalVol := Value;
end;

procedure TImpPedido.setFQntVolumes(const Value: string);
begin
  FQntVolumes := Value;
end;

procedure TImpPedido.setFSituacao(const Value: string);
begin
  FSituacao := Value;
end;

procedure TImpPedido.setFuf(const Value: string);
begin
  Fuf := Value;
end;

procedure TImpPedido.setFValorICMS(const Value: string);
begin
  FValorICMS := Value;
end;

procedure TImpPedido.setFValorICMSST(const Value: string);
begin
  FValorICMSST := Value;
end;

procedure TImpPedido.setFValorIPI(const Value: string);
begin
  FValorIPI := Value;
end;

procedure TImpPedido.setFValorParcela(const Value: string);
begin
  FValorParcela := Value;
end;

procedure TImpPedido.setFValorTotal(const Value: string);
begin
  FValorTotal := Value;
end;

procedure TImpPedido.setFValorTotalIPI(const Value: string);
begin
  FValorTotalIPI := Value;
end;

procedure TImpPedido.setFValorTotalLiq(const Value: string);
begin
  FValorTotalLiq := Value;
end;

procedure TImpPedido.setFValorUnit(const Value: string);
begin
  FValorUnit := Value;
end;

function TImpPedido.Split(const Str: string; Delimiter: Char;  Max: integer): TStringArray;
var
  Size, i: integer;
begin
  Size := 0;
  SetLength(Result, 1);
  Result[0] := '';
  for i := 1 to Length(Str) do
  begin
    if Str[i] = Delimiter then
    begin
      Inc(Size);
      if (Max <> 0) and (Size = Max) then
        Break;
      SetLength(Result, Size + 1);
    end
    else
      Result[Size] := Result[Size] + Str[i];
  end;
end;

end.
