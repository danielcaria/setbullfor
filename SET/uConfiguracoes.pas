unit uConfiguracoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, sEdit,
  sSpinEdit, iniFiles, ShellApi, Vcl.Mask, sMaskEdit, sCustomComboEdit, clSmtp,
  sToolEdit, clTcpClient, clMC;

type
  TfrmConfiguracoes = class(TForm)
    edtServidor: TEdit;
    edtPorta: TEdit;
    edtUsuario: TEdit;
    edtSenha: TEdit;
    lblServidor: TLabel;
    lblPorta: TLabel;
    lblUsuario: TLabel;
    lblSenha: TLabel;
    lblAutenticacao: TLabel;
    edtPathTransportadora: TEdit;
    lblPathArquivoVenda: TLabel;
    edtPathCliente: TEdit;
    lblPathCliente: TLabel;
    edtPathVendas: TEdit;
    lblPathImporta: TLabel;
    spndtTempoEnvio: TsSpinEdit;
    lblTempoEnvio: TLabel;
    btnSalvar: TButton;
    btnCancelar: TButton;
    btnSair: TButton;
    chkHabilitado: TCheckBox;
    Label1: TLabel;
    chAutentic: TCheckBox;
    edtServer: TEdit;
    edtPort: TEdit;
    edtUser: TEdit;
    edtPassword: TEdit;
    edtDatabase: TEdit;
    edtProvider: TEdit;
    lblServer: TLabel;
    lblPort: TLabel;
    lblUser: TLabel;
    lblPassword: TLabel;
    lblDatabase: TLabel;
    lblProvider: TLabel;
    lblSrvMail: TLabel;
    lblServidorDB: TLabel;
    edtEmailCopia: TEdit;
    lblEmailCopia: TLabel;
    edtPathLog: TEdit;
    edtEmissao: TsDateEdit;
    lblEmissao: TLabel;
    btnTeste: TButton;
    clsmtp: TclSmtp;
    procedure FormCreate(Sender: TObject);
    procedure btnSalvarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnTesteClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  frmConfiguracoes: TfrmConfiguracoes;

implementation

uses
  uMain;
{$R *.dfm}

procedure TfrmConfiguracoes.btnSairClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfrmConfiguracoes.btnSalvarClick(Sender: TObject);
var
  fConfig: TIniFile;
begin
  fConfig := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  with fConfig do
  begin
    WriteString('Email', 'Servidor', edtServidor.Text);
    WriteString('Email', 'Porta', edtPorta.Text);
    WriteString('Email', 'Usuario', edtUsuario.Text);
    WriteString('Email', 'Senha', edtsenha.Text);
    WriteString('Email', 'EMailCopia', edtEmailCopia.Text);
    WriteBool('Email', 'AutenticacaoTSLSSL', chAutentic.Checked);
    WriteString('Arquivo', 'Clientes', edtPathCliente.Text);
    WriteString('Arquivo', 'Transportadoras', edtPathTransportadora.Text);
    WriteString('Arquivo', 'Log', edtPathLog.text);
    WriteString('Arquivo', 'Vendas', edtPathVendas.Text);
    WriteInteger('Tempo', 'TempoEnvio', spndtTempoEnvio.Value);
    WriteDate('Emissao', 'EmissaoPedido', edtEmissao.Date);
    WriteBool('Tempo', 'Habilitado', chkHabilitado.Checked);
    WriteString('Database', 'server', edtServer.text);
    WriteString('Database', 'port', edtPort.text);
    WriteString('Database', 'user', edtUser.text);
    WriteString('Database', 'senha', edtPassword.text);
    WriteString('Database', 'Database', edtDatabase.text);
    WriteString('Database', 'Provider', edtProvider.text);
  end;
end;

procedure TfrmConfiguracoes.btnTesteClick(Sender: TObject);
begin
  clSmtp.Server := edtServidor.Text;
  clSmtp.Port := strToint(edtPorta.Text);
  clSmtp.UserName := edtUsuario.Text;
  clsmtp.Password := edtSenha.Text;

  if chAutentic.Checked then
  begin
    clSmtp.UseTLS := ctAutomatic;
  end
  else
  begin
    clSmtp.UseTLS := ctNone;
  end;
  try
  clSmtp.Open();
  clSmtp.Close();
  except on E: exception do
   ShowMessage('Erro ao conectar ' + E.Message);
  end;

end;

procedure TfrmConfiguracoes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ShowMessage('O Aplicativo ser� reiniciado!');
  frmMain.tmrBuscaDados.Enabled := True;
  ShellExecute(Handle, nil, PChar(Application.ExeName), nil, nil, SW_SHOWNORMAL);
  Application.Terminate;
end;

procedure TfrmConfiguracoes.FormCreate(Sender: TObject);
var
  fConfig: TIniFile;
begin
  frmMain.tmrBuscaDados.Enabled := False;
  fConfig := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  with fConfig do
  begin
    edtServidor.Text := ReadString('Email', 'Servidor', '');
    edtPorta.Text := ReadString('Email', 'Porta', '');
    edtUsuario.Text := ReadString('Email', 'Usuario', '');
    chAutentic.Checked := ReadBool('Email', 'AutenticacaoTSLSSL', true);
    edtSenha.Text := ReadString('Email', 'Senha', '');
    edtEmailCopia.Text := ReadString('Email', 'EMailCopia', '');
    edtPathCliente.Text := ReadString('Arquivo', 'Clientes', '');
    edtPathTransportadora.Text := ReadString('Arquivo', 'Transportadoras', '');
    edtPathVendas.Text := ReadString('Arquivo', 'Vendas', '');
    edtPathLog.Text := ReadString('Arquivo', 'Log', '');
    spndtTempoEnvio.Value := ReadInteger('Tempo', 'TempoEnvio', 1);
    chkHabilitado.Checked := ReadBool('Tempo', 'Habilitado', false);
    edtEmissao.Date := ReadDate('Emissao', 'EmissaoPedido', now);
    edtServer.text := ReadString('Database', 'server', '');
    edtPort.text := ReadString('Database', 'port', '');
    edtUser.text := ReadString('Database', 'user', '');
    edtPassword.text := ReadString('Database', 'senha', '');
    edtDatabase.text := ReadString('Database', 'Database', '');
    edtProvider.text := ReadString('Database', 'Provider', '');
  end;
end;

end.

