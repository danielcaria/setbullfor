unit uDataFactory;

interface
uses System.SysUtils, System.Variants, System.Classes, Data.DB, DBAccess, Uni,Winapi.Messages,Vcl.Dialogs;

  type
  TNotaFiscal = class
    private
    FNumeroNota : string;
    FChave : String;
    FPathXML : string;
    FPathPDF : string;
    protected
    procedure setNumeroNota(Const NumeroNota: string);
    procedure setChave(const chave : string);
    procedure setPathXML(Const pathxml : string);
    procedure setPathPDF(Const pathpdf : string);
    public
    property NotaFiscal : string read FNumeroNota write setNumeroNota;
    property Chave : string read FChave write setChave;
    property PathXML : string read FPathXML write setPathXML;
    property PathPDF : string read FPathPDF write setPathPDF;
    procedure addNota(const FPedido : string);

  end;
implementation
       uses uMain;
{ TNotaFiscal }

procedure TNotaFiscal.addNota(const FPedido : string);
var
  strSQL : string;
  FAddDB    : TUniSQL;
begin
  strSQL := Format('update epedido set eped_notafiscal = %s, eped_chavenfe = %s, eped_patch_pdf = %s, eped_patch_xml = %s where eped_codigo = %s',[quotedstr(FNumeroNota),quotedstr(Fchave),quotedstr(Fpathpdf),quotedstr(Fpathxml),quotedstr(fPedido)]);
  strSQL := StringReplace(strSQL,'"','',[rfReplaceAll]);
  FAddDB := TUniSQL.Create(nil);
  FAddDB.Connection := frmMain.UniConnection;
  FAddDB.SQL.Clear;
  FAddDB.SQL.Add(strSQL);
  FAddDB.Execute;
  FAddDB.Free;
end;

procedure TNotaFiscal.setChave(const chave: string);
begin
        FChave := chave;
end;

procedure TNotaFiscal.setNumeroNota(const NumeroNota: string);
begin
       FNumeroNota := NumeroNota;
end;

procedure TNotaFiscal.setPathPDF(const pathpdf: string);
begin
        FPathPDF := pathpdf;
end;

procedure TNotaFiscal.setPathXML(const pathxml: string);
begin
       FPathXML := pathxml;
end;

end.
