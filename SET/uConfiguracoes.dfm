object frmConfiguracoes: TfrmConfiguracoes
  Left = 0
  Top = 0
  Caption = 'Configura'#231#245'es'
  ClientHeight = 478
  ClientWidth = 806
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object lblServidor: TLabel
    Left = 13
    Top = 33
    Width = 48
    Height = 16
    Caption = 'Servidor'
  end
  object lblPorta: TLabel
    Left = 13
    Top = 59
    Width = 30
    Height = 16
    Caption = 'Porta'
  end
  object lblUsuario: TLabel
    Left = 13
    Top = 86
    Width = 43
    Height = 16
    Caption = 'Usu'#225'rio'
  end
  object lblSenha: TLabel
    Left = 13
    Top = 112
    Width = 36
    Height = 16
    Caption = 'Senha'
  end
  object lblAutenticacao: TLabel
    Left = 13
    Top = 139
    Width = 73
    Height = 16
    Caption = 'Autentica'#231#227'o'
  end
  object lblPathArquivoVenda: TLabel
    Left = 389
    Top = 51
    Width = 162
    Height = 16
    Caption = 'Cadastro de Transportadora'
  end
  object lblPathCliente: TLabel
    Left = 389
    Top = 19
    Width = 110
    Height = 16
    Caption = 'Arquivo de Clientes'
  end
  object lblPathImporta: TLabel
    Left = 389
    Top = 84
    Width = 107
    Height = 16
    Caption = 'Arquivo de Vendas'
  end
  object lblTempoEnvio: TLabel
    Left = 509
    Top = 148
    Width = 146
    Height = 16
    Caption = 'Tempo de Leitura e Envio'
  end
  object Label1: TLabel
    Left = 389
    Top = 114
    Width = 67
    Height = 16
    Caption = 'Arquivo Log'
  end
  object lblServer: TLabel
    Left = 17
    Top = 277
    Width = 48
    Height = 16
    Caption = 'Servidor'
  end
  object lblPort: TLabel
    Left = 17
    Top = 306
    Width = 30
    Height = 16
    Caption = 'Porta'
  end
  object lblUser: TLabel
    Left = 17
    Top = 335
    Width = 43
    Height = 16
    Caption = 'Usu'#225'rio'
  end
  object lblPassword: TLabel
    Left = 17
    Top = 365
    Width = 36
    Height = 16
    Caption = 'Senha'
  end
  object lblDatabase: TLabel
    Left = 17
    Top = 394
    Width = 53
    Height = 16
    Caption = 'Database'
  end
  object lblProvider: TLabel
    Left = 17
    Top = 423
    Width = 51
    Height = 16
    Caption = 'Provedor'
  end
  object lblSrvMail: TLabel
    Left = 17
    Top = 5
    Width = 309
    Height = 19
    Caption = 'Servidor de SMTP para envio de Mensagens'
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object lblServidorDB: TLabel
    Left = 17
    Top = 248
    Width = 199
    Height = 19
    Caption = 'Servidor de Banco de Dados'
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object lblEmailCopia: TLabel
    Left = 17
    Top = 174
    Width = 72
    Height = 16
    Caption = 'E-mail C'#243'pia'
  end
  object lblEmissao: TLabel
    Left = 492
    Top = 217
    Width = 163
    Height = 16
    Caption = 'Data emiss'#227'o maior ou igual'
  end
  object edtServidor: TEdit
    Left = 103
    Top = 30
    Width = 232
    Height = 24
    TabOrder = 1
    Text = 'edtServidor'
  end
  object edtPorta: TEdit
    Left = 103
    Top = 57
    Width = 121
    Height = 24
    TabOrder = 3
    Text = 'edtPorta'
  end
  object edtUsuario: TEdit
    Left = 103
    Top = 84
    Width = 232
    Height = 24
    TabOrder = 5
    Text = 'edtUsuario'
  end
  object edtSenha: TEdit
    Left = 103
    Top = 111
    Width = 232
    Height = 24
    TabOrder = 7
    Text = 'edtSenha'
  end
  object edtPathTransportadora: TEdit
    Left = 557
    Top = 48
    Width = 232
    Height = 24
    TabOrder = 2
    Text = 'edtPathTransportadora'
  end
  object edtPathCliente: TEdit
    Left = 557
    Top = 16
    Width = 232
    Height = 24
    TabOrder = 0
    Text = 'edtPathCliente'
  end
  object edtPathVendas: TEdit
    Left = 557
    Top = 81
    Width = 232
    Height = 24
    TabOrder = 4
    Text = 'edtPathVendas'
  end
  object spndtTempoEnvio: TsSpinEdit
    Left = 668
    Top = 145
    Width = 121
    Height = 24
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    SkinData.SkinSection = 'EDIT'
    MaxValue = 0
    MinValue = 0
    Value = 0
  end
  object btnSalvar: TButton
    Left = 531
    Top = 445
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 20
    OnClick = btnSalvarClick
  end
  object btnCancelar: TButton
    Left = 627
    Top = 445
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 21
  end
  object btnSair: TButton
    Left = 723
    Top = 445
    Width = 75
    Height = 25
    Caption = 'Sair'
    TabOrder = 22
    OnClick = btnSairClick
  end
  object chkHabilitado: TCheckBox
    Left = 509
    Top = 175
    Width = 280
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Leitura e envio habilitado'
    TabOrder = 11
  end
  object chAutentic: TCheckBox
    Left = 104
    Top = 142
    Width = 231
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Usar Autentica'#231#227'o TSL/SSL'
    TabOrder = 8
  end
  object edtServer: TEdit
    Left = 105
    Top = 273
    Width = 230
    Height = 24
    TabOrder = 14
    Text = 'edtServer'
  end
  object edtPort: TEdit
    Left = 105
    Top = 302
    Width = 230
    Height = 24
    TabOrder = 15
    Text = 'edtPort'
  end
  object edtUser: TEdit
    Left = 105
    Top = 331
    Width = 230
    Height = 24
    TabOrder = 16
    Text = 'edtUser'
  end
  object edtPassword: TEdit
    Left = 105
    Top = 361
    Width = 230
    Height = 24
    TabOrder = 17
    Text = 'edtPassword'
  end
  object edtDatabase: TEdit
    Left = 105
    Top = 390
    Width = 230
    Height = 24
    TabOrder = 18
    Text = 'edtDatabase'
  end
  object edtProvider: TEdit
    Left = 105
    Top = 420
    Width = 230
    Height = 24
    TabOrder = 19
    Text = 'edtProvider'
  end
  object edtEmailCopia: TEdit
    Left = 105
    Top = 171
    Width = 230
    Height = 24
    TabOrder = 10
    Text = 'edtEmailCopia'
  end
  object edtPathLog: TEdit
    Left = 557
    Top = 109
    Width = 232
    Height = 24
    TabOrder = 6
    Text = 'edtPathLog'
  end
  object edtEmissao: TsDateEdit
    Left = 668
    Top = 214
    Width = 121
    Height = 21
    AutoSize = False
    Color = clWhite
    EditMask = '!99/99/9999;1; '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 10
    ParentFont = False
    TabOrder = 13
    Text = '  /  /    '
    CheckOnExit = True
    GlyphMode.Blend = 0
    GlyphMode.Grayed = False
  end
  object btnTeste: TButton
    Left = 222
    Top = 201
    Width = 115
    Height = 25
    Caption = 'Testar Conexao'
    TabOrder = 12
    OnClick = btnTesteClick
  end
  object clsmtp: TclSmtp
    MailAgent = 'Clever Internet Suite'
    Left = 384
    Top = 200
  end
end
